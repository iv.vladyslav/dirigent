#!/bin/bash

source /dev/shm/golem/Commons.sh

#BAcha, je to Martinem pojmenovane od 0!

function SocketON() { CallFunction Devices/RelayBoards/Quido8-b/Switchboard-II RelayON $1; }
function SocketOFF() { CallFunction Devices/RelayBoards/Quido8-b/Switchboard-II RelayOFF $1; }

function SouthWestDownON(){ SocketON 5; } 
function SouthWestDownOFF(){ SocketOFF 5; } 

function SouthEastDownON(){ SocketON 6; } 
function SouthEastDownOFF(){ SocketOFF 6; } 

function SouthUpON(){ SocketON 7; } 
function SouthUpOFF(){ SocketOFF 7; } 

function DRPprobeON(){ SouthEastDownON; } 
function DRPprobeOFF(){ SouthEastDownOFF; }

function FastCamerasON(){ SouthWestDownON; } 
function FastCamerasOFF(){ SouthWestDownOFF; }


#Development issues
####################
#CallFunction Infrastructure/PowerGrid/Tokamak  FastCamerasON
