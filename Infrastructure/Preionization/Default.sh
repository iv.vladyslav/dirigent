#!/bin/bash

source /dev/shm/golem/Commons.sh

Gnome232=Gnome232-b

function DefineTable()
{
    CreateTable discharge.preionization
}

#function PreionHeaterON(){ ssh Discharge "source Drivers/Arduino8relayModul/driver.sh;RelayON 7"; }
#function PreionHeaterOFF(){ ssh Discharge "source Drivers/Arduino8relayModul/driver.sh;RelayOFF 7"; }



function GetReadyTheDischarge()
{
# Backward compatibility ..
    LogTheDeviceAction 
    GeneralTableUpdateAtDischargeBeginning "discharge.preionization"
    
    #powsup_heater=`cat $SHM0/Operation/Discharge/Preionization/Parameters/powsup_heater`
    powsup_heater=100
    num=$powsup_heater
    powsup_heater_format=$(printf "%03d\n" $powsup_heater)
    sum=0
    while [ $num -gt 0 ]
do
    mod=$(($num % 10))    #It will split each digits
    sum=$((sum + mod))   #Add each digit to sum
    num=$(($num / 10))    #divide num by 10.
done

echo $sum
chksum=$(printf "%x\n" $((10#$sum)))
echo $chksum
echo -ne "@0ANAP"$powsup_heater_format"E"$chksum"\n"|telnet $Gnome232 10001
sleep 3
echo -ne "@0AOUT19A\n"|telnet $Gnome232 10001

#echo "*B1OS1H"|telnet 192.168.2.240 10001 #PreionHeaterON

#powsup_accel=`cat $SHM0/Operation/Discharge/Preionization/Parameters/powsup_accel`
powsup_accel=100

echo "APPL $powsup_accel,2;OUTPut:IMMediate ON"|netcat -w 1 GWInstekPSW-b 2268

}

function SecurePostDischargeState()
{
#echo "*B1OS1L"|telnet 192.168.2.240 10001 #PreionHeaterOFF
echo -ne "@0AOUT099\n"|telnet $Gnome232 10001
echo "APPL 0,2;OUTPut:IMMediate OFF"|netcat -w 1 GWInstekPSW-b 2268
}



