#!/bin/bash

xterm -sb -bg black -fg white -hold -geometry 200x50+0+0 -title "Session setup: `basename $1 .setup` " -e "
function DoAtDirigent { 
read -e -p \"Do you want to \$1? [y/n] \" -i 'y' answer; 
#echo \$answer;
if [ \$answer == "y" ]; then 
    ssh -Y golem@golem 'export TERM=vt100;cd /golem/Dirigent/;./Dirigent.sh '\$2''; 
fi
}
function DoBashIt { 
read -e -p \"\$1? [y/n] \" -i 'y' answer; 
#echo \$answer;
if [ \$answer == "y" ]; then 
#    echo \$2
     bash -c /golem/Dirigent/Management/Setup/\$2.sh 
fi
}

echo Nastaveni noveho setup:
echo =======================
DoAtDirigent 'Reset previous session?' --resetup
ssh golem@golem 'cd /golem/Dirigent/;rm current.setup;ln -s '$1' current.setup';
DoAtDirigent 'Want to wake Up?' --wake
DoAtDirigent 'Ping It?' --ping
DoAtDirigent 'Ping It?' --ping
DoBashIt 'Do you want to setup monitors (xrandr)?' Monitors4GOLEM
DoBashIt 'Do you want to setup scopes (chromium)?' Scopes
DoBashIt 'Do you want to open the session? ' OpenCurrentSession
DoAtDirigent 'Pumping ON?' --pon
echo 'Nezapomen udelat DDS4SUJB!: Dgdds'
echo 'PAK Baking (Ctrl+Supr+B)'
echo 'pak Glow discharge (Ctrl+Supr+G)'
DoAtDirigent 'Pumping OFF?' --poff
DoAtDirigent 'Shutdown?' --shutdown
DoAtDirigent 'Close/reset session?' --resetup
echo end
read
"
