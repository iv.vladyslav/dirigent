xterm -sb -bg black -fg white -hold -geometry 200x50+0+0 -e "
function DoBashIt { 
read -e -p \"\$1? [y/n] \" -i 'y' answer; 
#echo \$answer;
if [ \$answer == "y" ]; then 
#    echo \$2
     bash -c /golem/Dirigent/Management/Setup/\$2.sh 
fi
}

DoBashIt 'Do you want to setup monitors (xrandr)?' Monitors4GOLEM
echo 'Nakonec je potreba sestrelit tento terminal ...'
"

kate -n /golem/Dirigent/Dirigent.sh /golem/Dirigent/Commons.sh /golem/Dirigent/current.setup &
google-chrome --password-store=basic --user-data-dir=$HOME/.config/google-chrome-Dirigent --new-window http://golem.fjfi.cvut.cz/shots/0 http://golem.fjfi.cvut.cz/wiki/ &
konsole --title "Dirigent" --profile Dirigent --tabs-from-file /golem/Dirigent/Management/Miscs/DirigentTabs & 
krusader --left /golem/Dirigent --right /golem/Dirigent/Setups  &
# Stay chrome on the main display
sleep 3
wmctrl -i -r `wmctrl -l|awk '{if ( $2 == 1 ){print $0}}'|grep Chrome|awk '{print $1}'` -e 0,1366,1920,1920,1080
sleep 1
#wmctrl -i -r `wmctrl -l|awk '{if ( $2 == 1 ){print $0}}'|grep Chrome|awk '{print $1}'` -b add,sticky

