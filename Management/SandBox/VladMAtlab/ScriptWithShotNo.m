ShotNo=39789

adress = ['http://golem.fjfi.cvut.cz/shots/' num2str(ShotNo) '/Devices/DASs/Papouch-Ji/'];

rad_ch = [ 1 2 3 4 5 6 7 8 10 11 13] ;
% try
%     temp = table2array(webread([adress 'ch' num2str(1) '.csv']));
%     data.loop = temp(:,2);
% end
for i = 2:12
    try
        temp = table2array(webread([adress 'ch' num2str(i) '.csv']));
        data.rad(:,rad_ch(i-1)) = temp(:,2);
    end
end
data.t_rad = temp(:,1)*1e3;

f = figure('visible', 'off');
% figure;
hold on;
for i = 2:12
    plot(data.t_rad, 1000*data.rad(:,rad_ch(i-1)));
end
grid on;
hold off
print -djpeg icon-fig.jpg
exit;
