"""Database model for ORM"""


def model_factory(app):
    """MySQL database ORM mapping"""
    fallback = (None,) * 2      # db, Shot
    try:
        from flask_sqlalchemy import SQLAlchemy
    except ImportError:
        return fallback
    try:
        db = SQLAlchemy(app)
    except:                     # TODO more specific error
        return fallback
    # reflect schema, fleshed out version of db.reflect() which has a bug with tables arg
    # see https://github.com/mitsuhiko/flask-sqlalchemy/commit/1968a92ec79a80f2c53c133d874be7d6f53f64c0
    db.Model.metadata.reflect(db.engine)

    class Shot(db.Model):
        '''ORM model representing rows of the shots table'''
        __tablename__ = 'shots'

    class AccessToken(db.Model):
        '''ORM model representing available access tokens'''
        __tablename__ = 'access_tokens'

    return db, Shot, AccessToken
