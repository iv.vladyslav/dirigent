# Test it
# bash 0622Doses.sh;head -1 Execute.sh |bash
# Execute it
# bash 0622Doses.sh;bash Execute.sh

column=D_integral_dose

echo "\
37477,0
37388,0
37421,0
37460,0
37412,0
39012,0
38086,1
38989,9
38487,0
38821,8
37901,0
37850,0
37741,0
37381,0
38361,2
38205,0
37980,1
38698,0
38421,11" > data.csv

export PGPASSWORD=`cat /golem/production/psql_password`
awk -F ',' '{print "psql -c \x27UPDATE shots SET \"'$column'\"="$2" WHERE shot_no=" $1"\x27 -q -U golem golem_database" }' data.csv |tee Execute.sh
