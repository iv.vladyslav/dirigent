ShotNo=0;
baseURL='http://golem.fjfi.cvut.cz/shots/';
diagnPATH='/Diagnostics/BasicDiagnostics/Results/U_loop.csv';
%Create a path to data
dataURL=strcat(baseURL,int2str(ShotNo),diagnPATH); 
% Write data from GOLEM server to a local file
urlwrite(dataURL,'LoopVoltage');
% Load data
data = load('LoopVoltage', '\t'); 
% Plot and save the graph 
f = figure('visible', 'off');
hold on
plot(data(:,1), data(:,2), '.') ;
xlabel('time [ms]')
ylabel('U_loop [V]')
hold off
print -djpeg plot.jpg
close(f)
exit;

