#!/bin/bash
source /dev/shm/golem/Commons.sh

whoami="Analysis/DataProcessing/Uloop/Uloop"



#ON/OFF: Discharge parameter:  --DataProcessing "off" 

function PostDischargeAnalysis()
{
    if [ `cat $SHM0/Production/Parameters/DataProcessing` == "off" ] ; then
        No_Analysis
    else
        Analysis
    fi
}  
 

function No_Analysis()
{ 
 echo 'Data processing off' > diagrow.html
}

function Analysis()
{
webpage=diagrow.html
squadra="Python3 Gnuplot Octave Matlab Mathematica"
rm log

    echo '<table border=1><tr class="data-flow"><th>Name</th>' > $webpage
    for i in $squadra; do
    echo "<th>$i</th>" >> $webpage
    done
    echo "</tr><tr><td><a href=$gitlabpath/`dirname $whoami` title="Gitlab4`dirname $whoami`">$gitlabicon</a><a href=`dirname $whoami` title="Directory">$diricon</a></td>" >> $webpage
    for i in $squadra; do
    echo "<td><a href=$gitlabpath/`dirname $whoami`/$i title="$i@Gitlab">
        <img src="$imgpath/$i.png" width=80px></img></a></td>" >> $webpage
    done
    echo "</tr><tr><td><img src=`dirname $whoami`/Uloop.png width='$namesize'></img></td>"  >> $webpage
    for i in $squadra; do
        echo -n $i in: >>log
        date "+%M:%S" >>log
        cd $i/
        echo $i
        bash Uloop.sh  # ToDoOrNotTodo
        cd -
        echo -n $i out: >>log
        date "+%M:%S" >>log
        echo "<td>
        <a href=`dirname $whoami`/$i/Uloop.html title="$i@GOLEM"><img src="`dirname $whoami`/$i/plot.jpg" width=$iconsize></img></a>
        </td>">> $webpage
    done
    echo "</tr></table>" >> $webpage
    cat log

}


#shot_no=35993;What=PostDischargeFinals;Where="Analysis/DataProcessing/DataProcessing.sh"; cp -r /golem/svoboda/Dirigent/`dirname $Where`/* /golem/database/operation/shots/$shot_no/`dirname $Where`/;cd /golem/database/operation/shots/$shot_no/`dirname $Where`;source `basename $Where` ;$What;
