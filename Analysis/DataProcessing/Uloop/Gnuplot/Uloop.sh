shot_no=`cat ../../../../shot_no`
echo  "set terminal jpeg;set datafile separator ',';set title 'Uloop #$shot_no';plot '< wget -q -O - http://golem.fjfi.cvut.cz/shots/"$shot_no"/Diagnostics/BasicDiagnostics/Results/U_loop.csv' u 1:2 w l t 'Uloop'" >UloopWithShotNo.gp
gnuplot UloopWithShotNo.gp >plot.jpg

webpage=Uloop.html
sas=`dirname $PWD|xargs basename`
echo "<h1>$sas: Data processing for GOLEM </h1>
<h2>Script:</h2><pre>" > $webpage
cat UloopWithShotNo.gp >>$webpage
echo "</pre><h2>Result:</h2><img src=plot.jpg width='30%'></img>" >>$webpage
