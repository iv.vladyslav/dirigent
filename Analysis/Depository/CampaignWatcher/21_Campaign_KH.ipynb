{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Campaign control\n",
    "\n",
    "Many tokamak measurements involve making the same discharge over and over again. This is particularly true in small tokamaks, where a single discharge is cheap and fast. Repeating the same discharge is at the heart of measuring on the *shot-to-shot basis*. For instance, in the absence of a reciprocating manipulator (which is expensive and demanding to maintain, but most of all too large for the GOLEM tokamak room), profiles of probe measurements are done by moving the probe manually between identical discharges. This leads to the requirement of a long row of identical plasmas.\n",
    "\n",
    "This notebook loads the number of discharges stored in `http://golem.fjfi.cvut.cz/shots/0/Production/Parameters/ScanDefinition` and assesses their reproducibility.\n",
    "\n",
    "## Import basic libraries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as pl\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load the list of campaign discharges"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from urllib.request import urlopen\n",
    "from urllib.error import HTTPError # recognise the error stemming from missing data\n",
    "\n",
    "shot_no = 0 #35470 #discharge which has a discharge list separated by commas\n",
    "\n",
    "#Define an exception which will be raised if the data is missing and stop the notebook execution\n",
    "class StopExecution(Exception):\n",
    "    def _render_traceback_(self):\n",
    "        pass\n",
    "\n",
    "def get_file(url, shot_no, silent=False):\n",
    "    URL = 'http://golem.fjfi.cvut.cz/shots/%i/%s' % (shot_no, url)\n",
    "    if not silent:\n",
    "        print(URL)\n",
    "        \n",
    "    try:\n",
    "        f = urlopen(URL)\n",
    "    except HTTPError: #no data was found\n",
    "        print('No data found at this address!')\n",
    "        return np.array([])\n",
    "    \n",
    "    if not silent:\n",
    "        print('File contents: ', f.read())\n",
    "        f = urlopen(URL) #rewind back to the file beginning\n",
    "    \n",
    "    try:\n",
    "        ret = np.loadtxt(f, delimiter=',')\n",
    "        return ret\n",
    "    except ValueError: # the data couldn't be converted to a row of numbers\n",
    "        print('No understandable data found!')\n",
    "        return np.array([])\n",
    "\n",
    "def computation_failed_plot():\n",
    "    fig = pl.figure(figsize=(2,2))\n",
    "    fig.tight_layout()\n",
    "    ax = pl.gca()\n",
    "    ax.plot([0], [0])\n",
    "    ax.set(yticks=[], xticks=[])\n",
    "    ax.text(0, 0, 'No campaign\\nspecified', horizontalalignment='center',\n",
    "            verticalalignment='center', fontsize=15)\n",
    "    pl.savefig('icon-fig.png')\n",
    "    return\n",
    "    \n",
    "# Load the discharge list and check if its non-empty\n",
    "shotlist = get_file('Production/Parameters/ScanDefinition', shot_no)\n",
    "if shotlist.size == 0:\n",
    "    print('The discharge list is empty!')\n",
    "    computation_failed_plot()\n",
    "    raise StopExecution\n",
    "shotlist = np.array([int(shot) for shot in shotlist]+[shot_no])\n",
    "print('Shotlist:', shotlist) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Assess the typical discharge beginning and end\n",
    "\n",
    "To set the X axis limits, it is useful to know the maximum discharge duration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the discharge beginnings and ends for every discharge in the campaign\n",
    "t = []\n",
    "for shot in shotlist:\n",
    "    t1 = get_file('Diagnostics/BasicDiagnostics/Results/t_plasma_start', shot, silent=True)\n",
    "    t2 = get_file('Diagnostics/BasicDiagnostics/Results/t_plasma_end', shot, silent=True)\n",
    "    t.append([float(t1), float(t2)])\n",
    "t = np.transpose(np.array(t))\n",
    "\n",
    "# Check if plasma was detected at least in one discharge\n",
    "if np.sum(t > 0) == 0: #all values are the default failure value, -1\n",
    "    print('No successful plasma detection! Using default values.')\n",
    "    t1 = 5\n",
    "    t2 = 15\n",
    "else:\n",
    "    # pick the earliest beginning and latest end\n",
    "    t1 = np.nanmin(t[0][ t[0] > 0 ])\n",
    "    t2 = np.nanmax(t[1][ t[1] > 0 ])\n",
    "\n",
    "print('Discharge duration: %.1f ms - %.1f ms' % (t1, t2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load standard basic diagnostics signals"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_basic_diag_data(shot_no):\n",
    "    '''\n",
    "    Return the standard diagnostics data for given shot:\n",
    "    time axis [s], loop voltage U_l [V], toroidal magnetic field B_t [T] and plasma current I_p [T].\n",
    "    '''\n",
    "    url = ( 'http://golem.fjfi.cvut.cz/shots/{}/Diagnostics/BasicDiagnostics/'\n",
    "            'basig_diagnostics_processed.csv'.format(shot_no) )\n",
    "    df = pd.read_csv(url, header=0, index_col='Time')\n",
    "    t = np.array(df.index) * 1e-3 #ms to s\n",
    "    data = np.transpose(np.array(df))\n",
    "    data[2:] *= 1e3 #kA to A\n",
    "    return t, data[0], data[1], data[2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Assess discharge reproducibility\n",
    "\n",
    "We consider a group of discharges identical if their loop voltage $U_l$, torodial magnetic field $B_t$ and plasma current $I_p$ are the same."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Prepare figure\n",
    "fig, axs = pl.subplots(3, 1, num=('Campaign basic diagnostics'), figsize=(8,8), sharex=True)\n",
    "    \n",
    "#Load and plot the basic diagnostics\n",
    "for shot in shotlist:\n",
    "    t, Ul, Bt, Ip = get_basic_diag_data(shot)\n",
    "    axs[0].plot(t*1000, Ul, label=str(shot))\n",
    "    axs[1].plot(t*1000, Bt, label=str(shot))\n",
    "    axs[2].plot(t*1000, Ip*1e-3, label=str(shot))\n",
    "    \n",
    "#Label the plot\n",
    "labels = ['$U_l$ [V]', '$B_t$ [T]', '$I_p$ [kA]']\n",
    "names = ['Loop voltage', 'Toroidal magnetic field', 'Plasma current']\n",
    "for i in range(3):\n",
    "    axs[i].legend(loc=1)\n",
    "    axs[i].set_xlim(t1-2, t2+5)\n",
    "    axs[i].grid(True)\n",
    "    axs[i].set_ylabel(labels[i])\n",
    "    axs[i].set_title(names[i])\n",
    "    axs[i].axhline(c='k')\n",
    "axs[2].set_xlabel('$t$ [ms]')\n",
    "axs[0].set_ylim(0, 20)\n",
    "\n",
    "# Save the figure\n",
    "pl.savefig('icon-fig')"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Edit Metadata",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
