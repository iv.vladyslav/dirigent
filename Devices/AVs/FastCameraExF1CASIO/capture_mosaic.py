#!/usr/bin/env python3
"""Script for capturing a high-speed movie of plasma and converting it to a mosaic image

Program requirements in PATH:
exf1ctrl
ffmpeg
convert  (from ImageMagick)
"""
import sys
import os
import re
import subprocess

blackdetect_regexp = re.compile(r'black_start:(?P<start>\d+\.?\d*) black_end:(?P<end>\d+\.?\d*)')
fps_regexp = re.compile(' (\d+\.\d+) fps')


def run_capture(movie_name='plasma', capture_seconds=10):
    input = (  # concat strings
        'c 8 3\n' +               # set high-speed movie mode with 1/1200 fps
        'e 1\n'  +               # set manual exposure
        'i 5\n'   +              # sets ISO 800
        'p 63\n'   +             # set shutter speed 1/32000
        'm {} {}\n'.format(capture_seconds, movie_name) +
        'q\n'                   # quits
    )
    result = subprocess.run(['exf1ctrl'], input=input, stdout=subprocess.PIPE,
                            universal_newlines=True)
    movie = movie_name + '.MOV'
    movie_saved = os.path.exists(movie)
    if movie_saved:
        print('Movie saved')
    else:
        print('Movie not saved! ExF1 control log follows')
        print(result.stdout)
    return movie_saved, movie


def iconize(input, icon_size='150x120',icon_name='icon.png'):
    subprocess.run(['convert', '-size', icon_size, input, icon_name])

def mov2mosaic(in_mov, out_png):
    # run the video through the blackdetect filter
    result = subprocess.run(['ffmpeg', '-i', in_mov, '-vf', 'blackdetect', '-f', 'null', '-'],
                            check=True, stderr=subprocess.PIPE, universal_newlines=True)
    fps_match = fps_regexp.search(result.stderr)
    fps = float(fps_match.group(1))
    blacks = [m.groupdict() for m in blackdetect_regexp.finditer(result.stderr)
              if m is not None]
    plasma_detected = len(blacks) >= 2
    if plasma_detected:
        plasma_start = float(blacks[0]['end'])
        plasma_end = float(blacks[1]['start'])
        plasma_frames = int(round(fps * (plasma_end - plasma_start)))
        subprocess.run(['ffmpeg', '-y', '-i', in_mov, '-vf',
                        'trim={}:,transpose=cclock,tile={}x1:padding=16'.format(plasma_start, plasma_frames),
                        '-frames', '1',  out_png], check=True, stderr=subprocess.PIPE)
    return plasma_detected

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('usage: {} plasma_film.png'.format(sys.argv[0]))
        sys.exit(1)
    out_png = sys.argv[1]
    movie_saved, movie = run_capture()
    if not movie_saved:
        sys.exit(1)
    plasma_detected = mov2mosaic(movie, out_png)
    os.remove(movie)            # not needed anymore
    with open('plasma_detected', 'w') as fout:
        fout.write(str(int(plasma_detected)))
    if plasma_detected:
        print('Plasma detected')
        iconize(out_png)
    else:
        print('No plasma detected')
        text = 'caption:NO PLASMA'
        iconize(text)

