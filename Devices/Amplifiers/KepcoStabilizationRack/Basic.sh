#!/bin/bash

source /dev/shm/golem/Commons.sh

# Get the name in the device (to ping etc ..):
#whoami=`echo $PWD|sed 's/\/golem\/Dirigent\///g'`/`basename $BASH_SOURCE .sh`

whoami="Devices/Amplifiers/KepcoStabilizationRack/Basic"
ThisDev=`dirname $whoami|xargs basename`



function WakeOnLan()
{
    LogIt $ThisDev
    shuf -i 0-5|head -1|xargs sleep # To Protect electrical grid
    echo "*B1OS3H"|telnet Quido8-a 10001 1>&- 2>&-; # Kepca dole
    sleep 1
    for i in `seq 1 4`; do echo $i; echo "*B1OS"$i"H"|telnet 192.168.2.240 10001; sleep 2;done

  
}

function SleepOnLan()
{
    LogIt $ThisDev
    for i in `seq 1 4`; do echo $i; echo "*B1OS"$i"L"|telnet 192.168.2.240 10001; sleep 2;done
    sleep 1
    echo "*B1OS3L"|telnet Quido8-a 10001 1>&- 2>&-; # Kepca dole
 
}


function PingCheck ()
{
    PingCheckSpecificIP Quido8-a Stabilization
    PingCheckSpecificIP Quido4-kepca Stabilization
}

