import sys
from pydcpf.appliances.AC250Kxxx import Device

if __name__ == '__main__':
    if len(sys.argv) < 1:
        print('usage: {} <voltage in V>'.format(sys.argv[0]))
    voltage = int(sys.argv[1])
    dev = Device(address=('192.168.2.232', 10001), internal_address=10,
                 interface_module='pydcpf.interfaces.socket_interface')
    dev.set_voltage(voltage)
    dev.set_output(True)
