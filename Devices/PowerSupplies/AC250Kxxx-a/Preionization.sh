#!/bin/bash

. /dev/shm/golem/Commons.sh

Devices="Interfaces/Gnome232-b/PreionizationPowSup"

Gnome232=Gnome232-b

function PingCheck () { 
Dev=`dirname $Devices`
    PingCheckSpecificIP `basename $Dev` `basename $Devices`
}

function Arming()
{
:
}

function ArmingZMB()
{
# Backward compatibility ..
    LogTheDeviceAction 
    GeneralTableUpdateAtDischargeBeginning "discharge.preionization"
    
    powsup_heater=`cat $SHM0/Operation/Discharge/Preionization/Parameters/powsup_heater`
    num=$powsup_heater
    powsup_heater_format=$(printf "%03d\n" $powsup_heater)
    sum=0
    while [ $num -gt 0 ]
do
    mod=$(($num % 10))    #It will split each digits
    sum=$((sum + mod))   #Add each digit to sum
    num=$(($num / 10))    #divide num by 10.
done

echo $sum
chksum=$(printf "%x\n" $((10#$sum)))
echo $chksum
echo -ne "@0ANAP"$powsup_heater_format"E"$chksum"\n"|telnet $Gnome232 10001
sleep 3
echo -ne "@0AOUT19A\n"|telnet $Gnome232 10001


}

function SecurePostDischargeStateZMB()
{
echo -ne "@0AOUT099\n"|telnet $Gnome232 10001
}
