#/bin/sh



function comm_exec_gw_psw() {
    python3 -c "from vacuum.psw_gw_instek import GWInstekPSW; ret = GWInstekPSW('192.168.2.75').$1"
}

function get_psw_voltage() {
    comm_exec_gw_psw "get_voltage(); print(ret)"
}

function set_psw_voltage() {
    comm_exec_gw_psw "set_voltage($1)"
}

function get_psw_output() {
    comm_exec_gw_psw "get_output(); print(ret)"
}

function set_psw_output() {
    comm_exec_gw_psw "set_output($1)"
}
