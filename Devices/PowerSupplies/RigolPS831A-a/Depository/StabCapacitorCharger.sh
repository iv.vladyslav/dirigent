#!/bin/bash
SUBDIR=Devices
ThisDev=AdHocPowerSupply/StabCapacitorCharger
#192.168.2.60

TheDevice=RigolPS831Aa

function SendCommandToTheDevice()
{
    #echo $1|netcat -w 1 $TheDevice 5555
    echo $1|netcat -w 1 $TheDevice 5555
}



function PrepareDischarge() 
{ 
    $LogFunctionPassing; 
    SendCommandToTheDevice ":INST CH2;:CURR 2;:VOLT `cat /dev/shm/golem/ActualShot/param1`;:OUTP CH2,ON"
    echo ":INST CH2;:CURR 2;:VOLT 15;:OUTP CH2,ON"|netcat -w 1 $TheDevice 5555
    #SendCommandToTheDevice ":INST CH2;:CURR 2;:VOLT 7;:OUTP CH2,ON"
    echo OK
}

function PostDisch()
{
    $LogFunctionGoingThrough
    SendCommandToTheDevice ":OUTP CH2,OFF"
    echo OK
}

function PostDischargeFinals()
{
    echo OK
}


function CommonInitDischarge()
{
    echo OK
}


#echo ":INST CH1;:CURR 2;:VOLT 5;:OUTP CH1,ON"|netcat -w 1 192.168.2.60 5555

