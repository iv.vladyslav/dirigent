#!/bin/bash
ThisDev=HVsubstite
SHM="/dev/shm/golem"

source $SHM/Commons.sh
source $SHM/Tools.sh

COMMAND="netcat -w 1 $ThisDev 5555"


function OpenSession()
{
    echo OK
}

function CloseSession()
{
    echo OK
}


function OpenDischarge()
{
    #channel 2
    echo ":INST CH2;:CURR 2;:VOLT 30;:OUTP CH2,ON"|netcat -w 1 HVsubstitute 5555
    #channel 3
    #echo ":INST CH3;:CURR 0.1;:VOLT -30;:OUTP CH3,ON"|netcat -w 1 HVsubstitute 5555
    echo OK
}

function PostDisch()
{
    #channel 2
    echo ":OUTP CH2,OFF"|netcat -w 1 HVsubstitute 5555 # Tests on RigolPS
    #channel 2
    #echo ":OUTP CH3,OFF"|netcat -w 1 HVsubstitute 5555 # Tests on RigolPS
} 

#DEPOT

#echo ":INST CH1;:CURR 2;:VOLT 5;:OUTP CH1,ON"|netcat -w 1 192.168.2.60 5555

