#!/bin/bash


. /dev/shm/golem/Commons.sh

TheDevice=GWinstekPSW-b


# SendCommandToTheDevice "*idn?"

function SendCommandToTheDevice()
{
    echo $1|netcat -w 1 $TheDevice 2268
}




function ArmingZMB() 
{ 

    #powsup_accel=`cat $SHM0/Operation/Discharge/Preionization/Parameters/powsup_accel`
    powsup_accel=80
    echo "APPL $powsup_accel,2;OUTPut:IMMediate ON"|netcat -w 1 $TheDevice 2268
}

function SecurePostDischargeStateZMB()
{
    $LogFunctionGoingThrough
    SendCommandToTheDevice "OUTPut:IMMediate OFF"
}

