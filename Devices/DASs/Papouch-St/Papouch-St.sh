#!/bin/bash



BASEDIR="../.."
source $BASEDIR/Commons.sh


SUBDIR="DASs"
ThisDev="Papouch-St"

source $SHMS/Drivers/PapouchDAS1210/driver.sh

papouch_ip=Papouch-St

shot_no=`CurrentShotDataBaseQuerry shot_no`


data_dir=$SHM0/$SUBDIR/$ThisDev


function WakeOnLan()
{
    curl -sd 'pw=1' http://Energenie_LANpower-a/login.html >/dev/null
    curl -sd "cte3=1" http://Energenie_LANpower-a/status.html >/dev/null
}

function SleepOnLan()
{
    curl -sd 'pw=1' http://Energenie_LANpower-a/login.html >/dev/null
    curl -sd "cte3=0" http://Energenie_LANpower-a/status.html >/dev/null
}


function DASArming()
{
    sleep 1
    arm_papouch_das $papouch_ip
}



function RawDataAcquiring()
{
WEBPATH="$PWD"
LastChannelToAcq=$1


    echo "<html><body>" > das.html
    WebRecDas "<h1>The GOLEM tokamak DAS $ThisDev for Shot #$shot_no </h1>"
    WebRecDas "<h2><a href="http://golem.fjfi.cvut.cz/shots/$shot_no/DASs/$ThisDev/">Data dir</a><h2/>"
    WebRecDas "<img src='plot.jpg'/><br></br>"

    LogItColor 4 "$ThisDev: Start of acquiring"
    for i in `seq $LastChannelToAcq` ; do
            echo -ne ACQ: $i: ;
            read_channel_papouch_das $papouch_ip $i $data_dir/ch$i.csv 40e-3; 
    done
    WebRecDas "</body></html>"
    
    echo -n "set terminal jpeg;unset xtics;set size 1,1;set origin 0,0;set multiplot layout $LastChannelToAcq,1 columnsfirst scale 1.1,1;set datafile separator ',';" >/tmp/foo; for i in `seq 1 $LastChannelToAcq`; do echo -n plot \''ch'$i'.csv'\' u 1:2';';done >>/tmp/foo;echo " unset multiplot" >>/tmp/foo; cat /tmp/foo|gnuplot > plot.jpg
    convert -resize 200x200 plot.jpg icon.png

    
    
    }
 
