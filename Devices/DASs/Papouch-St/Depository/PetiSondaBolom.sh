#!/bin/bash
SUBDIR=DASs
ThisDev=PetiSondaBolom
SHM="/dev/shm/golem"
WEBPATH="$PWD"
source $SHM/Commons.sh
source $SHM/Tools.sh
source $SHM/Drivers/PapouchDAS1210/driver.sh

papouch_ip=Papouch-St

shot_no=`CurrentShotDataBaseQuerry shot_no`


data_dir=$SHM0/$SUBDIR/$ThisDev/

function Arming()
{
    sleep 1
    DASsOpenSession # Workarround
    mkdir -p $data_dir

    arm_papouch_das $papouch_ip

    echo OK
}


function DASsOpenSession()
{
    $LogFunctionGoingThrough
    echo OK
}

#diags=('null' 'channel_1' 'channel_2' 'channel_3' 'channel_4' 'channel_5' 'channel_6'  'channel_7' 'channel_8')
diags=('null' '1-Lim' '2-OLim' '3-BPP' '4-LP'  'bolo13' 'bolo14'  'bolo15' 'bolo16'  'bolo17' 'bolo18'  'bolo19' 'bolo20')
icondiags=('1-Lim' '2-OLim' '3-BPP' '4-LP' )


function PostDischargeFinals()
{
    echo "<html><body>" > das.html
    WebRecDas "<h1>The GOLEM tokamak $ThisDev@$papouch_ip for Shot #$shot_no </h1>"
    WebRecDas "<h2><a href="http://golem.fjfi.cvut.cz/shots/$shot_no/DASs/$ThisDev/">Data dir</a><h2/>"
    WebRecDas "<img src='graph1.png'/><br></br>"

    LogItColor 4 "$ThisDev: Start of acquiring"
    for i in `seq 1 12` ; do
        diag_id=${diags[$i]}
        if [ "$diag_id" != 'null' ]; then 
            echo -ne ACQ: $i: $diag_id,;read_channel_papouch_das $papouch_ip $i $data_dir/$diag_id.csv 40e-3; 
            echo "set terminal jpeg;set datafile separator ',';set title '$shot_no';set xrange [3000e-6:15000e-6];set style data dots;set ylabel '$diag_id  [V]'; set xlabel 'Time [s]';set output '$diag_id.jpg';plot '"$diag_id".csv' u 1:2 w l title ''"|gnuplot
            WebRecDas "<h2>$ThisDev:$diag_id (raw voltage signal)</h2>"
            WebRecDas "<img src='$diag_id.jpg'/></br>"
            WebRecDas "<a href='http://golem.fjfi.cvut.cz/shots/$shot_no/DASs/$ThisDev/"$diag_id".csv'/>Data link</a></br>"
        fi
    done
    WebRecDas "</body></html>"
    
    #set title '$shot_no';
    BASE1="set datafile separator ',';unset key;set xrange [3000e-6:15000e-6];set style data lines;set format y '%3.1f';set multiplot;set size 1,0.25;set origin 0,0.75;set ylabel 'Lim';plot '1-Lim.csv';set origin 0,0.5;unset xtics;set ylabel 'OLim';plot '2-OLim.csv';set origin 0,0.25;unset xtics;set ylabel 'BPP';plot '3-BPP.csv';set origin 0,0.0;unset xtics;set ylabel 'LP';plot '4-LP.csv'" 
	echo "set terminal png;$BASE1"|gnuplot >graph1.png
	convert -resize 150 graph1.png icon.png
    LogIt "$ThisDev: End of acquiring"
    echo OK
}




function SandBox() 
{
    #set title '$shot_no';
    BASE1="set datafile separator ',';unset key;set xrange [0:22000e-6];set style data lines;set format y '%3.1f';set multiplot;set size 1,0.25;set origin 0,0;set ylabel 'Lim';plot '1-Lim.csv';set origin 0,0.25;unset xtics;set ylabel 'OLim';plot '3-OLim.csv';set origin 0,0.5;unset xtics;set ylabel 'BPP';plot '5-BPP.csv';set origin 0,0.75;unset xtics;set ylabel 'LP';plot '6-LP.csv'" 
	echo "set terminal png;$BASE1"|gnuplot >graph1.png
	convert -resize 150 graph1.png icon.png
}





function PostDisch()
{
    $LogFunctionGoingThrough
    echo OK
}

