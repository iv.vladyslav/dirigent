#!/bin/bash

source /dev/shm/golem/Commons.sh


whoami="Devices/DASs/NIoctopus/20_PRPL_MatejI"
ThisDev=`dirname $whoami|xargs basename`
DataFile=NIdata.lvm


source Universals.sh


data_dir=$SHM0/`dirname $whoami`


function RawDataAcquiring()
{
    local shot_no=`cat $SHMS/shot_no`
    timeout=20
    while ssh golem_daq@$ThisDev.golem "! test -s /home/golem_daq/$DataFile";
    do
        if [ "$timeout" == 0 ]; then
        LogItColor 1 "ERROR: Timeout while waiting for the file from $ThisDev"
        exit 1
    fi
    sleep 1
    echo $timeout s to wait for $ThisDev files
    ((timeout--))
    done

    #shot_no=`cat $BASEDIR/shot_no`
    LogItColor 4 "$ThisDev: Start of acquiring"
    scp -o ConnectTimeout=1 golem_daq@NIoctopus.golem:/home/golem_daq/$DataFile .; \
 
    
    rm tmp
    echo  "set terminal png size 1600, 1000;set output 'ScreenShotAll.png';set bmargin 0;set tmargin 0;set lmargin 10;set rmargin 3;unset xtics;unset xlabel;set multiplot layout 8,2 columnsfirst title 'GOLEM Shot $shot_no';set xrange [*:*];set yrange [*:*];set style data dots;unset ytics;"> tmp; for i in `seq 9 24`; do echo "set ylabel '$((i-8))';plot '$DataFile' u 1:$((i+1)) t '' w l lc 1;" >> tmp;done
    cat tmp|gnuplot 1>/dev/null 2>/dev/null
   	convert -resize 200x200 ScreenShotAll.png rawdata.jpg


}


