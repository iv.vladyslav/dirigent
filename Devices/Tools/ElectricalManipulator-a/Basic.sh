#!/bin/bash

source /dev/shm/golem/Commons.sh

whoami=`echo $PWD|sed 's/\/golem\/Dirigent\///g'`/`basename $BASH_SOURCE .sh`
ThisDev=`dirname $whoami|xargs basename`

function WakeOnLan()
{
    LogIt $ThisDev
    shuf -i 0-5|head -1|xargs sleep # To Protect electrical grid
    echo "*B1OS4H"|telnet 192.168.2.254 10001 1>&- 2>&-; #F33
    sleep 10
    python3 probe_homing.py wake
    
}

function SleepOnLan()
{
    LogIt $ThisDev
    #python3 probe-homing.py sleep
    echo "*B1OS4L"|telnet 192.168.2.254 10001 1>&- 2>&-; #F33

}


function PingCheck ()
{
    WaitForDevice D-Linkcamera-a $ThisDev
    WaitForDevice D-Linkcamera-b $ThisDev
    WaitForDevice Motor_Controller-a $ThisDev
    WaitForDevice Motor_Controller-b $ThisDev
    

}
