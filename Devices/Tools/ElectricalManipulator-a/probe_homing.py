#!/usr/bin/env python
# coding: utf-8

# In[12]:


import requests as rq
import sys
import time
import binascii
import numpy as np
rq.packages.urllib3.connection.HTTPConnection.default_socket_options = [(6,3,1)]


# In[24]:


global position
address=32
path = "/golem/database/www/plot_app/plot_app/"
position = int(np.loadtxt(f'{path}rad_position'))#-1 #mm

def post(index,subindex,value):
        rq.post(f'http://192.168.2.{address}/od/{index}/{subindex}',data=f'{value}',
                    headers={'Content-Type': 'application/x-www-form-urlencoded'})
        time.sleep(0.001)


def get(index,subindex,show):
        output = rq.get(f'http://192.168.2.{address}/od/{index}/{subindex}').text
        if(show==True):
            print(f'object: {index}:{subindex}-------------')
            print('hexadec: '+ str(output))
            print('decimal:' + str(int(output[1:-1],16)))
            print('binary: '+ str(bin(int(output[1:-1], 16))[2:]))
        time.sleep(0.001)
        return output


def inic():
    post("2030","00",'"000000C8"') # radialni ma 000000C8
    time.sleep(0.01)
    post("2031","00",'"000003E8"')
    time.sleep(0.01)
    post("6075","00",'"000001F4"')
    time.sleep(0.01)
    post("3202","00",'"00000008"')
    time.sleep(0.01)


def homing_rad():
    global position
    position = -1
    inic()
    time.sleep(0.1)

    post("60FE","01",'"00000000"')              # brake off
    time.sleep(0.1)
    post("6060","00",'"03"')
    time.sleep(0.1)
    post("6042","00",'"00F0"')  #target speed
    time.sleep(0.1)
    post("606D","00",'"0010"')  # velocity window
    time.sleep(0.1)
    post("606E","00",'"0010"')  # time window
    time.sleep(0.1)
    post("60FF","00",'"00000010"') # this speed is OK for radial one for profile velocity mode
    time.sleep(0.1)

    post("607E","00",'"00"')    #polarity
    time.sleep(0.1)

    post("6040","00",'"0006"')
    time.sleep(0.1)
    post("6040","00",'"0007"')
    time.sleep(0.1)
    post("6040","00",'"000F"')
    time.sleep(0.5)

    while(True):
        time.sleep(0.01)
        if(get('60FD','00',False) == '"00040000"'):
            #print('ON')
            post("6040","00",'"0000"')                  # turn everything off
            time.sleep(0.01)
            post("60FE","01",'"00000001"')              # brake on
            time.sleep(0.01)
            post("6040","00",'"0080"') # remove error
            break
    return position

def shut_down():
    inic()
    post("6040","00",'"0000"')                  # turn everything off
    post("60FE","01",'"00000001"')              # brake on
    post("6040","00",'"0080"') # remove error


# In[26]:


if(sys.argv[1] == 'wake'):
    try:
        inic()
        shut_down()
    except Exception as e:
        print(e)
        
elif(sys.argv[1] == 'sleep'):
    try:
        inic()
        homing_rad()
        shut_down()

        f =  open(f'{path}rad_position', 'w+')
        f.write(f'{position}')
        f.close
    except Exception as e:
        print(e)
else:
    print("somehting were wrong with manipulator")

