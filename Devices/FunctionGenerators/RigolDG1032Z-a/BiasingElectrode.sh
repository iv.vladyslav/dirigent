#!/bin/bash
source /dev/shm/golem/Commons.sh

whoami="Devices/FunctionGenerators/RigolDG1032Z-a/BiasingElectrode"

ThisDev=RigolDG1032Z-a.golem
#echo "*IDN?"|netcat -w 1 192.168.2.171 5555


COMMAND="netcat -w 1 $ThisDev 5555"

# Everything at Operation/Discharge/Stabilization/Stabilization.sh


function GetReadyTheDischarge ()
{
mkdir Parameters
    GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh

    raw=`cat $SHM0/Production/Parameters/diagnostics_biasingelectrode`
    
    raw=$(echo $raw | tr -s ',' ' ')
raw=$(echo $raw | tr -s ';' ' ')

time=()
value=()
a=0
for i in $raw
    do
      if [[ a%2 -eq 0 ]]    
      then
        time+=($i)
      else
        value+=($i)
      fi
    let "a+=1"
    done

echo Time points: ${time[@]}
echo Values: ${value[@]}


sign () { echo "$(( $1 < 0 ? -1 : 1 ))"; } #absolute value

#looks for the max value
Max=0
for m in ${value[@]} 
    do
      m=$(( $m * $(sign "$m") ))    
      if [[ $m -gt $Max ]]
      then 
         Max=$m 
      fi
    done
echo Max value: $Max

#generate sequence of points for the frequency generator
sequence=()
j=0
l=1
for ((i=${time[0]};i<=${time[-1]};i+=100))
   do
     k=${value[$l-1]}
     sequence+=$(python -c "print(round(${k}.0 / ${Max}.0,1),',')") #strange but functional
     if [[ $i -eq ${time[$l]} ]]
     then
        let "l+=1"
     fi
    let "j+=1"
   	done
c=${sequence[@]}
echo Final sequence: $c


echo ":OUTP2 OFF"|netcat -w 1 192.168.2.171 5555 #Turn off output off ch2
sleep 0.1;
echo "Waveform pars"
echo ":SOUR2:APPL:ARB 10000,$Max,0"|netcat -w 1 192.168.2.171 5555 #Arbitrary 
echo ":SOUR2:DATA VOLATILE, $c"|netcat -w 1 192.168.2.171 5555 #predefined function
echo "Burst mode"
echo ":SOUR2:BURS ON"|netcat -w 1 192.168.2.171 5555
echo ":SOUR2:BURS:MODE:TRIG;:SOUR2:BURS:TRIG:SOUR EXT;:SOUR2:BURS:NCYC 1 "|netcat -w 1 192.168.2.171 5555 #Set trigger to manual/external and number of 
sleep 0.2;
echo ":SOUR2:BURS:TDEL 0.00${time[0]}"|netcat -w 1 192.168.2.171 5555 #Set time delay (in seconds)

}


function Arming()
{

echo "And output"
echo ":OUTP2 ON"|netcat -w 1 192.168.2.171 5555 #Turn on output of 

#========Frequency generator settings===========

    
}
   
   
   function PostDischargeAnalysis
{
    echo ":OUTP2 OFF"|netcat -w 1 192.168.2.171 5555 #Turn off output off ch2
      
}      
      
