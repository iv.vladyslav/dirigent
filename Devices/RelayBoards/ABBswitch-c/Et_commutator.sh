#!/bin/bash

source /dev/shm/golem/Commons.sh

Devices="Devices/ITs/Bt_Ecd_management.link/Rasp_Bt_Ecd"

RASP=Bt_Ecd_management
SSHatRASP="ssh -Y golem@$RASP"
SOURCE='source Rasp_Bt_Ecd.sh'


function SwitchIt
{
      ssh $RASP "source Rasp_Bt_Ecd.sh;RelayON $1"  
      mRelax
      ssh $RASP "source Rasp_Bt_Ecd.sh;RelayOFF $1"  
}


function ClockWise() { SwitchIt 9; }
function OFF() { SwitchIt 10; }
function AntiClockWise() { SwitchIt 11; }


function PingCheck () { :; }

