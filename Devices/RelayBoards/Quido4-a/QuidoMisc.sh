#!/bin/bash

source /dev/shm/golem/Commons.sh

# Device functions
function 12V_24V_intosystem_ON  { echo "*B1OS1H"|telnet QuidoMisc 10001 1>/dev/null 2>/dev/null; }
function 12V_24V_intosystem_OFF { echo "*B1OS1L"|telnet QuidoMisc 10001 1>/dev/null 2>/dev/null; }

# Device service for the GOLEM operation
function GetReadyTheDischarge
{
    12V_24V_intosystem_ON
}

function SecurePostDischargeState
{
    12V_24V_intosystem_OFF
}


#Development issues
####################

# source /golem/Dirigent/Devices/RelayBoards/Quido4-a/QuidoMisc.sh;12V_24V_intosystem_ON
