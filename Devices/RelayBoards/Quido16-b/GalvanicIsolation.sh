#!/bin/bash

source /dev/shm/golem/Commons.sh

QuidoModul='telnet QuidoGalvanicIsolation 10001'

function RelayON() { echo "*B1OS"$1"H"|$QuidoModul   1>/dev/null 2>/dev/null; }
function RelayOFF(){ echo "*B1OS"$1"L"|$QuidoModul  1>/dev/null 2>/dev/null; }

function RadiometerON(){ RelayON 4; }
function RadiometerOFF(){ RelayOFF 4; }

function RigolMSO5204-d_ON { RelayON 3; }
function RigolMSO5204-d_OFF { RelayOFF 3; }


