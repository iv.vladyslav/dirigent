#!/bin/bash

source /dev/shm/golem/Commons.sh

QuidoModul='telnet QuidoVacuumRelays 10001'

function RelayON() { echo "*B1OS"$1"H"|$QuidoModul   1>/dev/null 2>/dev/null; }
function RelayOFF(){ echo "*B1OS"$1"L"|$QuidoModul  1>/dev/null 2>/dev/null; }

function TMP-aStandbyON(){ RelayON 6; }
function TMP-aStandbyOFF(){ RelayOFF 6; }
function TMP-aON(){ RelayON 7; }
function TMP-aOFF(){ RelayOFF 7; }

function TMP-bStandbyON(){ RelayON 3; }
function TMP-bStandbyOFF(){ RelayOFF 3; }
function TMP-bON(){ RelayON 4; }
function TMP-bOFF(){ RelayOFF 4; }

function Vent-aON(){ RelayON 8; }
function Vent-aOFF(){ RelayOFF 8; } #1:Sever
function Vents-aON(){ RelayON 9; }
function Vents-aOFF(){ RelayOFF 9; } #1:Sever
function Valves-aOFF(){ Vent-aOFF;Vents-aOFF;  }
function Valves-aON(){  Vent-aON;Vents-aON;  }

function Vent-bON(){ RelayON 5; }
function Vent-bOFF(){ RelayOFF 5; } #2 Jih
function Vents-bON(){ RelayON 10; }
function Vents-bOFF(){ RelayOFF 10; } #2 Jih
function Valves-bOFF(){ Vent-bOFF;Vents-bOFF;  }
function Valves-bON(){  Vent-bON;Vents-bON;  }

function GasH2ON(){ RelayON 12; }
function GasH2OFF(){ RelayOFF 12; }

function GasHeON(){ RelayON 13; }
function GasHeOFF(){ RelayOFF 13;  }



