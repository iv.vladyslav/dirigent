import socket


ACK = b'\x06'
NAK = b'\x15'
ENQ = b'\x05'


class TPG262GNOME232(object):

    def __init__(self, ipaddress='192.168.2.246', port=10001):
        self.sock = socket.create_connection((ipaddress, port))

    def recv(self, bufsize=64):
        buf = b''
        while not buf.endswith(b'\r\n'):
            buf += self.sock.recv(bufsize)
        return buf

    def transmit(self, msg):
        req = msg + b'\r'
        self.sock.sendall(req)
        ret = self.recv()
        code = ret.strip()
        if code != ACK or code == NAK:
            raise IOError('Communication not acknowledged')
    
    def query(self, msg):
        self.transmit(msg)
        self.sock.send(ENQ)
        ret = self.recv()
        return ret.strip()

    def get_pressure(self, channel):
        msg = 'PR{}'.format(channel).encode()
        ret = self.query(msg)
        status, value = ret.strip().split(b', ')
        if status != b'0':
            raise ValueError('Measurement error code {}'.format(status))
        value = float(value)
        return value
