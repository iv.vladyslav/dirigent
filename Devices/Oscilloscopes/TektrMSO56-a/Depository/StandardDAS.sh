#!/bin/bash
SUBDIR=DASs
SHM="/dev/shm/golem"
ThisDev=TektrMSO56-a/StandardDAS
QUERRY="netcat -w 5 `dirname $ThisDev` 4000"
COMMAND="netcat -q 1 `dirname $ThisDev` 4000"
WEBPATH="$PWD"

# MOUNT: 192.168.2.116, tek_drop, golem, tokamak

source $SHM/Commons.sh
source $SHM/Tools.sh


shot_no=`CurrentShotDataBaseQuerry shot_no`
#shot_no=31839 # just for testing


#echo ""|netcat -q 1  TektrMSO56-a 4000

function OpenSession()
{
    echo "RECALL:SETUP 'StandardDAS.set'"|$COMMAND
    echo ":ACQUIRE:MODE HIRes"|$COMMAND
    echo ":HORIZONTAL:MODE:MANUAL"|$COMMAND
    echo ":HORIZONTAL:MODE:MANUAL:CONFIGURE RECORDLENGTH"|$COMMAND
    echo ":HORIZONTAL:MODE MANUAL"|$COMMAND 
    echo ":HORIZONTAL:MODE:SAMPLERATE 1e6"|$COMMAND
    echo ":HORIZONTAL:MODE:SCALE 2.4e-3"|$COMMAND
    echo ":HORIZONTAL:POSITION 3 "|$COMMAND
    echo ":SAVEON:WAVEFORM:FILEFORMAT SPREADSheet"|$COMMAND
    echo ":SAVEon:FILE:DEST 'O:/'"|$COMMAND
    echo ":SAVEON:WAVEform ON"|$COMMAND
    echo ":SAVEon:TRIG ON"|$COMMAND
    echo ":SAVEON:IMAGE ON"|$COMMAND
    echo ":SAVEON:FILE:NAME 'TektrMSO56'"|$COMMAND
    echo ':SAVEON:WAVEform:SOURCE ALL'|$COMMAND
        
    for i in `seq 1 6`; do 
        echo ":DISplay:GLObal:CH$i:STATE ON"|$COMMAND
        echo ":CH$i:BANDWIDTH 200e3"|$COMMAND
    done
    
    echo ":CH1:SCALE 5;:CH1:OFFSET 20"|$COMMAND
    echo ":CH2:SCALE 130e-3;:CH2:OFFSET 520e-3"|$COMMAND
    echo ":CH3:SCALE 100e-3;:CH3:OFFSET 0"|$COMMAND
    echo ":CH4:SCALE 50e-3;:CH4:OFFSET 200e-3"|$COMMAND
    echo ":CH5:SCALE 200e-3;:CH5:OFFSET 800e-3"|$COMMAND
    echo ":CH6:SCALE 750e-3;:CH6:OFFSET 3000e-3"|$COMMAND
    
    echo "CH1:LABel:NAME 'U_loop,coil'"|$COMMAND
    echo "CH2:LABel:NAME 'U_Bt,coil'"|$COMMAND
    echo "CH3:LABel:NAME 'U_Rog,coil'"|$COMMAND
    echo "CH4:LABel:NAME 'U_Photod'"|$COMMAND
    echo "CH5:LABel:NAME 'Null'"|$COMMAND
    echo "CH6:LABel:NAME 'Trigger'"|$COMMAND
    
    echo "FPANEL:PRESS SINGLESEQ"|$COMMAND
    echo "TRIGGER:A:MODE NORMAL"|$COMMAND
    echo "TRIGGER:A:TYPE EDGE "|$COMMAND
    echo "TRIGGER:A:LEVEL:CH6 4"|$COMMAND
    echo "TRIGGER:A:EDGE:SOURCE CH6"|$COMMAND
    
    echo "MATH:ADDNEW 'MATH1'"|$COMMAND
    echo "MATH:MATH1:TYPE ADVANCED"|$COMMAND
    echo "MATH:MATH1:DEFine 'INTG(Ch2)*70.42'"|$COMMAND
    echo "MATH:MATH1:LABel:NAMe 'B_t'"|$COMMAND
    echo "MATH:MATH1:VUNIT 'T'"|$COMMAND
    echo ":DISPLAY:WAVEVIEW1:MATH:MATH1:AUTOSCALE 1"|$COMMAND
    
    echo "MATH:ADDNEW 'MATH2'"|$COMMAND
    echo "MATH:MATH2:TYPE ADVANCED"|$COMMAND
    echo "MATH:MATH2:DEFine '-INTG(Ch3)*5.3e6-Ch1/9.7e-3'"|$COMMAND
    echo "MATH:MATH2:LABel:NAMe 'I_p'"|$COMMAND
    echo "MATH:MATH2:VUNIT 'A'"|$COMMAND
    echo ":DISPLAY:WAVEVIEW1:MATH:MATH2:AUTOSCALE 1"|$COMMAND

 
    # -INTG(Ch3)*5.3e6-Ch1/9.7e-3
    # INTG(Ch2)*70.42

    echo OK
}


function CommonInitDischarge()
{
    echo OK
}

function CloseSession()
{
    echo OK
}

   #diags=('null' 'loop_voltage' 'toroidal_field_coil_voltage' 'rogowski_coil_voltage' 'photodiode_alpha' )
   
   
#echo "FPANEL:PRESS SINGLESEQ"|netcat -q 1  TektrMSO56-a 4000
#echo ""|netcat -q 1  TektrMSO56-a 4000

   
function SingleSeq()
{
    echo "FPANEL:PRESS SINGLESEQ"|$COMMAND
}   


function ForceTrig()
{
    echo "FPANEL:PRESS FORCetrig"|$COMMAND
}

function Arming()
{
    sleep 1
    OpenSession # Workarround, zlobi to ..
    rm -f /home/golem/tektronix_drop/*.csv
    rm -f /home/golem/tektronix_drop/*.png
    #mkdir -p $SHM0/$SUBDIR/$ThisDev/
    echo ":DISplay:GLObal:CH1:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH2:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH3:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH4:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH5:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH6:STATE ON"|$COMMAND


    SingleSeq
    echo OK
}


Nodiags=5
trigger=6
Nomaths=2
diags=('null' 'LoopVoltageCoil' 'BtCoil' 'RogowskiCoil' 'LeyboldPhotodiodeNoFilter' 'InnerQuadrupole' 'Trigger')
#diags=('null' 'LoopVoltageCoil' 'BtCoil' 'RogowskiCoil' 'BPP' 'Tolim' 'Trigger')
maths=('null' 'BtCoil' 'RogowskiCoil')


function Web() 
{
    cp $dirigent_dir/DASs/TektrMSO56-a/StandardDAS.ipynb . # will be better
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" StandardDAS.ipynb
    # jupyter-nbconvert index.ipynb # Zatim tady nejde ...
    convert -resize $icon_size ScreenShot.png graph.png 
    convert /$SHM/Management/imgs/TektrMSO56_icon.jpg -gravity center graph.png -gravity center +append icon_.png
    convert -bordercolor Black -border 2x2 icon_.png icon.png
    cp $dirigent_dir/DASs/TektrMSO56-a/das.jpg . # will be better
    

}


function PostDischargeFinals()
{
    $LogFunctionGoingThrough
    getdata
    echo ":DISplay:GLObal:CH2:STATE OFF"|$COMMAND
    echo ":DISplay:GLObal:CH3:STATE OFF"|$COMMAND
    echo ":DISplay:GLObal:CH5:STATE OFF"|$COMMAND
    echo ":DISplay:GLObal:CH6:STATE OFF"|$COMMAND
    GetOscScreenShot
    Web 
    echo OK

}
    

	

function getdata ()
{
    for i in `seq 1 3`; do Relax; done # Waiting for data to be stored
    PathTo=`basename $ThisDev`
    LogIt 3 "$PathTo: Start of acquiring"
    ls -all /home/golem/tektronix_drop/* > $SHM0/DASs/$PathTo/ls-all
    cp `ls  -d /home/golem/tektronix_drop/TektrMSO56_ALL_*.csv |tail -n 1` TektrMSO56_ALL.csv
    
    
    
    tail -q -n +13  TektrMSO56_ALL.csv |awk -F ',' '{print $1","$2}' > $SHM0/DASs/$PathTo/LoopVoltageCoil_raw.csv
    tail -q -n +13  TektrMSO56_ALL.csv |awk -F ',' '{print $1","$3}' > $SHM0/DASs/$PathTo/BtCoil_raw.csv
    tail -q -n +13  TektrMSO56_ALL.csv |awk -F ',' '{print $1","$4}' > $SHM0/DASs/$PathTo/RogowskiCoil_raw.csv
    tail -q -n +13  TektrMSO56_ALL.csv |awk -F ',' '{print $1","$5}' > $SHM0/DASs/$PathTo/LeyboldPhotodiodeNoFilter_raw.csv
    tail -q -n +13  TektrMSO56_ALL.csv |awk -F ',' '{print $1","$6}' > $SHM0/DASs/$PathTo/NULL_raw.csv
    tail -q -n +13  TektrMSO56_ALL.csv |awk -F ',' '{print $1","$7}' > $SHM0/DASs/$PathTo/Trigger_raw.csv
    tail -q -n +13  TektrMSO56_ALL.csv |awk -F ',' '{print $9","$10}' > $SHM0/DASs/$PathTo/BtCoil_integrated.csv
    tail -q -n +13  TektrMSO56_ALL.csv |awk -F ',' '{print $12","$13}' > $SHM0/DASs/$PathTo/RogowskiCoil_integrated.csv
    mv  `ls -d /home/golem/tektronix_drop/*|grep png|grep TektrMSO56` $SHM0/DASs/$PathTo/ScreenShotAll.png
    
    LogIt "$PathTo: End of acquiring"
    
  
}


function GetOscScreenShot()
{
    #null: /usr/local/lib/python2.7/dist-packages/pkg_resources/py2_warn.py:22: UserWarning: Setuptools will stop working on Python 2
    /usr/bin/python $SHM/Drivers/TektrMSO5/main56-a.py save_screenshot $PWD/ScreenShot.png 2>/dev/null
    echo OK
}


function PostDischarge()
{
:
}

function sandbox ()
{

    echo "TRIGGER:A:TIME 1e-3"|$COMMAND


}


# ************* ZMBS **********************************

function zmbs ()
{
    echo ":data:source ch1"|$COMMAND
    XINCR=`echo ":WFMPRE:XINCR?"|$QUERRY`;echo $XINCR > XINCR.osc

}

function getdataOldFW_zmb ()
{
    for i in `seq 1 3`; do Relax; done # Waiting for data to be stored
    PathTo=`basename $ThisDev`
    LogIt 3 "$PathTo: Start of acquiring"
    ls -all /home/golem/tektronix_drop/* > $SHM0/DASs/$PathTo/ls-all
    
    tail -n +10  `ls -d /home/golem/tektronix_drop/*|grep TektrMSO56_ch1` > $SHM0/DASs/$PathTo/LoopVoltageCoil_raw.csv
    tail -n +10  `ls -d /home/golem/tektronix_drop/*|grep TektrMSO56_ch2` > $SHM0/DASs/$PathTo/BtCoil_raw.csv
    tail -n +10  `ls -d /home/golem/tektronix_drop/*|grep TektrMSO56_ch3` > $SHM0/DASs/$PathTo/RogowskiCoil_raw.csv
    tail -n +10  `ls -d /home/golem/tektronix_drop/*|grep TektrMSO56_ch4` > $SHM0/DASs/$PathTo/LeyboldPhotodiodeNoFilter_raw.csv
    tail -n +10  `ls -d /home/golem/tektronix_drop/*|grep TektrMSO56_ch5` > $SHM0/DASs/$PathTo/NULL_raw.csv
    tail -n +10  `ls -d /home/golem/tektronix_drop/*|grep TektrMSO56_ch6` > $SHM0/DASs/$PathTo/Trigger_raw.csv
    tail -n +10  `ls -d /home/golem/tektronix_drop/*|grep TektrMSO56_math1` > $SHM0/DASs/$PathTo/BtCoil_integrated.csv
    tail -n +10  `ls -d /home/golem/tektronix_drop/*|grep TektrMSO56_math2` > $SHM0/DASs/$PathTo/RogowskiCoil_integrated.csv
    mv  `ls -d /home/golem/tektronix_drop/*|grep png|grep TektrMSO56` $SHM0/DASs/$PathTo/ScreenShotAll.png
    
    LogIt "$PathTo: End of acquiring"
    
  
}
