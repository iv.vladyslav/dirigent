#!/bin/bash

source /dev/shm/golem/Commons.sh

whoami="Devices/Oscilloscopes/TektrMSO58-a/21_RunAways_JCetal"

source Universals.sh


#Channels:
Nodiags=5
trigger=6
Nomaths=2
# pocitano od nuly, prvni musi byt 'null'



function OpenSession()
{
    echo ":ACQUIRE:MODE HIRes;
    :HORIZONTAL:MODE:SAMPLERATE 1e6;
    :HORIZONTAL:MODE:SCALE 2.4e-3;
    FPANEL:PRESS SINGLESEQ;
    TRIGGER:A:MODE NORMAL;
    TRIGGER:A:TYPE EDGE ;
    TRIGGER:A:LEVEL:CH8 4;
    TRIGGER:A:EDGE:SOURCE CH8;
    :SAVEon:TRIG ON;
    :SAVEON:WAVEform ON;
    :SAVEON:WAVEFORM:FILEFORMAT SPREADSheet;
    :SAVEon:FILE:DEST 'L:/';
    :SAVEON:IMAGE ON;
    :SAVEON:FILE:NAME 'TektrMSO58';
    :SAVEON:WAVEform:SOURCE ALL"|$COMMAND 1>/dev/null 2>/dev/null
    
    #for i in `seq 1 8`;do
    #       echo ":CH$i:SCALE 5;:CH$i:OFFSET 0;CH$i:LABEL:NAME 'ch$i';:DISplay:GLObal:CH$i:STATE ON"|$COMMAND  1>/dev/null 2>/dev/null
    #done
    
    echo "
    :CH1:SCALE 4;:CH1:OFFSET 0;:DISplay:GLObal:CH1:STATE ON;:CH1:TERMINATION 1.0E+6;
    :CH2:SCALE 0.05;:CH2:OFFSET 0;:DISplay:GLObal:CH2:STATE ON;:CH2:TERMINATION 50;
    :CH3:SCALE 0.1;:CH3:OFFSET 0;:DISplay:GLObal:CH3:STATE ON;:CH3:TERMINATION 50;
    :CH4:SCALE 0.05;:CH2:OFFSET 0;:DISplay:GLObal:CH4:STATE ON;:CH4:TERMINATION 50;
    :CH5:SCALE 0.05;:CH3:OFFSET 0;:DISplay:GLObal:CH5:STATE ON;:CH5:TERMINATION 50;
    :CH6:SCALE 1;:CH6:OFFSET 0;:DISplay:GLObal:CH6:STATE ON;:CH6:TERMINATION 50;
    :CH7:SCALE 10;:CH7:OFFSET 0;:DISplay:GLObal:CH7:STATE OFF;:CH7:TERMINATION 1.0E+6;
    :CH8:SCALE 2;:CH8:OFFSET 0;:DISplay:GLObal:CH8:STATE ON;:CH8:TERMINATION 1.0E+6;
    :CH1:LABel:NAME 'U_loop-b';
    :CH2:LABel:NAME 'YAP-a';
    :CH3:LABel:NAME 'CeBr-a';
    :CH4:LABel:NAME 'CebR-b';
    :CH5:LABel:NAME 'NaITl-a';
    :CH6:LABel:NAME 'LYSO';
    :CH7:LABel:NAME 'null';
    :CH8:LABel:NAME 'Trigger'"|$COMMAND 1>/dev/null 2>/dev/null
    
    ExternDataAvailabilityTest
    
    PrepareFilesToSHMs $SHMS Devices/`dirname $Drivers`

    
}


function Arming()
{

    LogTheDeviceAction

    rm -f /home/golem/tektronix_drop/TektrMSO58*.csv
    rm -f /home/golem/tektronix_drop/TektrMSO58*.png
    echo "
    :SAVEon:TRIG ON;
    :SAVEON:WAVEform ON;
    :SAVEON:WAVEFORM:FILEFORMAT SPREADSheet;
    :SAVEon:FILE:DEST 'L:/';
    :SAVEON:IMAGE ON;
    :SAVEON:FILE:NAME 'TektrMSO58';
    `for i in 1 2 3 4 5 6 7 8; do echo :DISplay:GLObal:CH$i:STATE ON;done`
    :SAVEON:WAVEform:SOURCE ALL"|$COMMAND 1>/dev/null 2>/dev/null

	SingleSeq
}
   


   
   
   
   




function RawDataAcquiring()
{
    # for i in `seq 1 5`; do echo Call from Petiprobe $i;sleep 1; done
    getdata "1 2 3 4 5 6 7"

    echo "
    :DISplay:GLObal:CH1:STATE ON;
    :DISplay:GLObal:CH2:STATE ON;
    :DISplay:GLObal:CH3:STATE ON;
    :DISplay:GLObal:CH4:STATE ON;
    :DISplay:GLObal:CH5:STATE ON;
    :DISplay:GLObal:CH6:STATE ON;
    :DISplay:GLObal:CH7:STATE OFF;
    :DISplay:GLObal:CH8:STATE OFF"|$COMMAND  1>/dev/null 2>/dev/null
    


    #GetOscScreenShot
}
    
