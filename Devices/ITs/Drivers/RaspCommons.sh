# RASPs issues: (to be executed at RASPs)
# **********************************************************

source Commons.sh


function MountCentralSHMEnvironment()
{

    mkdir -p $SHM
    sshfs golem@$DirigentServer:$SHM/ $SHM/ 
    df|grep golem
}   

function CallCentralFunction
{
local where=$1
local what=$2

    ssh $GM "source $SW_dir/$where.sh;$what"
}


