#!/usr/bin/python3

import redpitaya_scpi as rp_scpi


def arm_red_pitaya(address: str,outp: int=1):
    rp_s = rp_scpi.scpi(address)

    # turns on output 1
    rp_s.tx_txt('OUTPUT{}:STATE ON'.format(outp))
    rp_s.tx_txt('SOUR{}:TRIG:INT'.format(outp))
    rp_s.tx_txt('DIG:PIN LED1,1')


arm_red_pitaya('192.168.2.169')
