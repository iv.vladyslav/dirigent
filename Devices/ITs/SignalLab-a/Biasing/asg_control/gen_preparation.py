#!/usr/bin/python3

import numpy as np
import sys
import time
from utils import array_to_string
import waveform_generation as wg
import redpitaya_scpi as rp_scpi


class Setup:
    __slots__ = ()

    buff_freq = 25 # this corresponds to buff duration 20 ms
    address = '192.168.2.169'  # this should be checked on scpi startup
    max_amplitude = 10
    wave_form = 'arbitrary'
    bursts = 1


def gen_setup(rp_s, freq: int, ampl: int, wave_form: str, outp: int = 1):
    """
    Gen setup does the basic setup of the ASG on RedPitaya.
    :param rp_s:
    :param freq:
    :param ampl:
    :param wave_form:
    :param outp:
    :return:
    """

    # resets the generator to default
    rp_s.tx_txt('GEN:RST')
    # sets the arbitrary waveform
    rp_s.tx_txt('SOUR{}:FUNC '.format(outp) + str(wave_form).upper())

def original_wf_generation():
    wave_form = 'arbitrary'
    freq = 50
    ampl = 1

    N = 16383
    t = np.linspace(0, 1, N + 1)  # *2*math.pi

    x = np.sin(2 * np.pi * t)  # + 1/3*np.sin(3*t)
    y = 1 / 2 * np.sin(t) + 1 / 4 * np.sin(4 * t)

    waveform_ch_10 = []

    for n in x:
        waveform_ch_10.append("{:.5f}".format(n))
    waveform_ch_1 = ", ".join(map(str, waveform_ch_10))

    return waveform_ch_1


def convert_params_to_correct_range(parameter: float, max_amplitude: int = 10):
    """
    As the buffer of the ASG may only contain floats from -1 to 1,
    the request from command line, that is in physical units is then
    processed here so that from 'wg.generate_waveform' a waveform
    with correct range is produced.
    :param parameter:
    :param max_amplitude:
    :return:
    """
    conv_param = parameter / max_amplitude

    return conv_param


def main(console_vars: list = sys.argv, outp: int = 1):
    print(console_vars)
    console_vars = [float(v) for v in console_vars[1:]]

    ampl = Setup.max_amplitude  # maximal amplitude in V
    wave_form = Setup.wave_form
    freq = Setup.buff_freq  # sets the frequency of fast analog output
    address = Setup.address

    harmonic_amplitude = convert_params_to_correct_range(console_vars[2])
    harmonic_offset = convert_params_to_correct_range(console_vars[3])

    # generates waveform
    wf_arr = wg.generate_waveform(harmonic_start=console_vars[0],
                                  harmonic_stop=console_vars[1],
                                  harmonic_amplitude=harmonic_amplitude,
                                  harmonic_offset=harmonic_offset,
                                  harmonic_freq=console_vars[4],
                                  buff_freq=freq)

    wf_str = array_to_string(wf_arr)

    rp_s = rp_scpi.scpi(address)  # creates a connection to the SCPI server

    # performs a basic setup of the ASG
    gen_setup(rp_s,
              freq=freq,
              ampl=ampl,
              wave_form=wave_form,  # this is the name of the waveform
              outp=outp)

    # loads the arbitrary waveform to ASG buffer
    rp_s.tx_txt('SOUR{}:TRAC:DATA:DATA '.format(outp) + wf_str)

    # sets the output frequency
    rp_s.tx_txt('SOUR{}:FREQ:FIX '.format(outp) + str(freq))

    # sets the maximum amplitude
    rp_s.tx_txt('SOUR{}:VOLT '.format(outp) + str(ampl))

    # enables the burst mode :D let's see if it works the way we want it
    rp_s.tx_txt('SOUR{}:BURS:STAT BURST'.format(outp))
    rp_s.tx_txt('SOUR{}:BURS:NCYC 1'.format(outp))


    # rp_s.tx_txt('SOUR{}:TRIG:INT'.format(outp))


if len(sys.argv) < 6:
    print("RedPitaya: Not enough arguments!")
else:
    main(sys.argv)

