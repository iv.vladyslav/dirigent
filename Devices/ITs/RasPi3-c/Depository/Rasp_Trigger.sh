#!/bin/bash

# To be executed at RASPs

whoami="Devices/RASPs/Charger/Trigger"

source Commons.sh


source Drivers/Arduino8relayModul/driver.sh

function TriggerCloneBatteriesON(){ RelayON 13; }
function TriggerCloneBatteriesOFF(){ RelayOFF 13; }


function PrepareSession()
{
    mkdir -p $SHM
    sshfs golem@Dirigent:$SHM/ $SHM/
    
    #CommonPrepareSessionIssues

}	


function PrepareSessionEnv@SHM()
{
    MountCentralSHMEnvironment 
}	



function GetReadyTheDischarge()
{
    LogTheDeviceAction
    EngageBats
    TriggerCloneBatteriesON
}

function SecurePostDischargeState()
{
    DisEngageBats
    TriggerCloneBatteriesOFF

}



function EngageBats()
{
    for i in `seq 1 16`; do  echo "*B1OS"$i"H"|telnet 192.168.2.233 10001 1>/dev/null 2>/dev/null;uRelax;done
}

function DisEngageBats()
{
    for i in `seq 1 16`; do  echo "*B1OS"$i"L"|telnet 192.168.2.233 10001 1>/dev/null 2>/dev/null;uRelax;done
}


# RsyncDeviceFromDirigent

function HWTrigger()
{
# Back compatibility ..
local SUBDIR=RASPs
local ThisDev=Discharge

    mkdir -p $SHM0/$SUBDIR/$ThisDev

    $LogFunctionGoingThrough
    
    #Tch3_request=`cat $SHMP/TBt` (0 nefunguje ????, musí být min 1, vyresit pres if)
    if [ -f "$SHMP/TBt" ]; then
        Tch3_request=$(echo `cat $SHMP/TBt`|bc) # workarround
    else 
         Tch3_request=0
    fi
    echo $Tch3_request > $SHM0/Devices/RASPs/Discharge/t_bt_discharge_request
    echo $Tch3_request > $SHM0/Devices/RASPs/Discharge/Tch3_request
    LogIt "Going with trigger Tch3 - TBt $Tch3_request"

    if [ -f "$SHMP/Tcd" ]; then
        Tch4_request=$(echo `cat $SHMP/Tcd`-`cat $SHMP/TBt`|bc)
    else 
         Tch4_request=0
    fi
    echo $Tch4_request > $SHM0/Devices/RASPs/Discharge/t_cd_discharge_request
    echo $Tch4_request > $SHM0/Devices/RASPs/Discharge/Tch4_request
    LogIt "Going with trigger Tch4 - Tcd $Tch4_request"

    #Tch5_request=`cat $SHMP/Tch5`
    if [ -f "$SHMP/Tch5" ]; then
        Tch5_request=$(echo `cat $SHMP/Tch5` - `cat $SHMP/Tcd`|bc)
    else 
        Tch5_request=0
    fi
    echo $Tch5_request > $SHM0/Devices/RASPs/Discharge/Tch5_request
    echo $Tch5_request > $SHM0/Devices/RASPs/Discharge/t_innerstab_discharge_request
    LogIt "Going with trigger Tch5 - Tist $Tch5_request"

 
    
    
    echo "void setup() {                
  pinMode(2, OUTPUT);                                                                                                                                                                                                                         
  pinMode(3, OUTPUT);                                                                                                                                                                                                                         
  pinMode(4, OUTPUT);                                                                                                                                                                                                                         
  pinMode(5, OUTPUT);                                                                                                                                                                                                                         
  pinMode(6, OUTPUT);                                                                                                                                                                                                                         
  pinMode(7, OUTPUT);
  // DAS
  digitalWrite(2, HIGH);   
  // system is not working properly for 0 request, 1 us delay (everywhere) is not a problem .. for now.
  delayMicroseconds($((Tch3_request+1))); 
  // Bt
  digitalWrite(3, HIGH);   
  delayMicroseconds($((Tch4_request+1))); 
  // Et
  //digitalWrite(4, HIGH);//workarround 0520
  //digitalWrite(5, HIGH);//workarround 0620
  digitalWrite(6, HIGH);
  // Stabilizace Vnitrni quadrupol
  delayMicroseconds($((Tch5_request+1)));
    digitalWrite(7, HIGH);
  delay(3);
  digitalWrite(2, LOW);   
  digitalWrite(3, LOW);   
  digitalWrite(4, LOW);   
  digitalWrite(5, LOW);   
  digitalWrite(6, LOW);   
  digitalWrite(7, LOW);   
}
void loop() {
}
  " > main.ino       
    
    cp main.ino ~/Drivers/Arduino/TriggerOverUSB/
    make -C ~/Drivers/Arduino/TriggerOverUSB upload 1>/dev/null 2>/dev/null

}



function Trigger()
{
    $LogFunctionGoingThrough
    HWTrigger
}


function TriggerTestInfinity()
{
    EngageBats
    TriggerCloneBatteriesON
    while [ 1 ]; do HWTrigger; echo "Go ..";sleep 1;done
    DisEngageBats
    TriggerCloneBatteriesOFF

}

function TriggerTestC()
{
    EngageBats
    TriggerCloneBatteriesON
    for i in `seq 1 100`; do HWTrigger; echo "Go ..$i";sleep 1;done
    DisEngageBats
    TriggerCloneBatteriesOFF
}


function TriggerTest()
{
    EngageBats
    TriggerCloneBatteriesON
    for i in `seq 1 1`; do HWTrigger; echo "Go ..$i";sleep 1;done
    DisEngageBats
    TriggerCloneBatteriesOFF
}


function DummyTrigger()
{
    EngageBats
    TriggerCloneBatteriesON
    for i in `seq 1 1`; do HWTrigger; echo "Go ..$i";sleep 1;done
}
