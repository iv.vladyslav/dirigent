# requires root priviliges for creating the eeprom block device

# based on reading the unipi/evok source code

import os
import struct
from pprint import pprint
from time import sleep


def read_eeprom(base='/sys/class/i2c-dev/i2c-1/device/'):
    #TODO parametrize address

    eeprom_file = base + '1-0050/eeprom'

    if not os.path.exists(eeprom_file):
        # create interface for 24c02 chip at address 0x50
        with open(base + 'new_device', 'w') as f:
            f.write('24c02 0x50')
    sleep(0.5)  # wait for interface to be created

    with open(eeprom_file, 'rb') as f:
        eeprom = f.read(256)   # 256*8-bit block

    # according to Table 4 in UniPi tech docs
    info = {
        'UniPi identification': eeprom[0xe0:0xe0+2],
        'UniPi version': eeprom[0xe2:0xe2+2],
        'AI1 coefficient': struct.unpack('!f', eeprom[0xf0:0xf0+4])[0],
        'AI2 coefficient': struct.unpack('!f', eeprom[0xf4:0xf4+4])[0],
        }
    # remove interface to be safe
    with open(base + 'delete_device', 'w') as f:
        f.write('0x50')

    return info


if __name__ == '__main__':
    info = read_eeprom()
    pprint(info)
