import six.moves.tkinter as tk
from six.moves.tkinter_font import Font


from .controller import RelaysController


class Application(tk.Frame):

    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.font = Font(size=45)
        self.buttons = {}

        self.controller = RelaysController()

        for row in range(8):
                button = tk.Button(self, text='Rele {}'.format(row+1),
                        command=lambda row=row: self.click_relay_button(row),
                        font=self.font,
                        )
                button.grid(row=row, column=1)
                self.buttons[(row)] = button
                self.colorize_relay_button(row)

    def click_relay_button(self, row):
        self.controller.relay_flip(row+1)
        self.colorize_relay_button(row)

    def colorize_relay_button(self, row):
        button = self.buttons[(row)]
        color = 'green' if self.controller.relay_is_on(row+1) else 'red'
        button.configure(bg=color, activebackground=color)


if __name__ == '__main__':
    root = tk.Tk()
    app = Application(master=root)
    app.mainloop()
