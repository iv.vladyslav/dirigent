from .MCP230XX import MCP230XX
import RPi.GPIO as GPIO


class RelaysController:

	def __init__(self):
		self.MCPRelays = MCP230XX('MCP23008', 0x25, '16bit')

		for i in range(8):
			self.MCPRelays.set_mode(i, 'output', 0x68, )

		GPIO.setmode(GPIO.BCM)
		GPIO.setup(18, GPIO.OUT)
		self.p = GPIO.PWM(18, 400)

		self.relay_state=[False]*9
		#print(self.relay_state)

	def relay(self, num, value):
		pin = 8-num
		self.relay_state[num] = value
		self.MCPRelays.output(pin, value)

	def relay_flip(self, num):
		self.relay(num, not self.relay_state[num])

	def relay_is_on(self, num):
		return self.relay_state[num]

	def analogOut(self, level):
		dc = level*10

		#DEBUG
		#print(dc)

		self.p.ChangeDutyCycle(dc)
