import MCP230XX
import MCP342X

import smbus



'''
def pin(relay):
    if relay>9:
        return 0
        
    if relay<1
        return 7
        
	return 8-relay
'''
import RPi.GPIO as GPIO

class unipi:
	def __init__(self):
		self.MCPRelays=MCP230XX.MCP230XX('MCP23008', 0x20, '16bit')

		for i in range(8):
			self.MCPRelays.set_mode(i, 'output', 0x68, )

		#for ADC, not working yet
		#self.MPCADC=MCP342X.MCP342x(smbus.SMBus(1), )

		GPIO.setmode(GPIO.BCM)
		GPIO.setup(18, GPIO.OUT)
		self.p = GPIO.PWM(18, 400)

		self.relay_state=[False]*9
		#print(self.relay_state)


	def relay(self, num, value):
		pin = 8-num
		self.relay_state[num] = value
		self.MCPRelays.output(pin, value)
		
	def relay_flip(self, num):
		self.relay(num, not self.relay_state[num])
		
	def relay_is_on(self, num):
		return self.relay_state[num]
		
	def analogOut(self, level):
		dc = level*10

		#DEBUG
		#print(dc)

		self.p.ChangeDutyCycle(dc)
