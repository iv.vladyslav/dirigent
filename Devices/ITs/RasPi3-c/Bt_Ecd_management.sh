#!/bin/bash

source /dev/shm/golem/Commons.sh

RASP=Bt_Ecd_management
SSHatRASP="ssh -Y golem@$RASP"
SOURCE='source Rasp_Bt_Ecd.sh;source Rasp_Trigger.sh'


# Device service for the GOLEM operation (kotvy/anchors) START
##############################################################

function PrepareSessionEnv@SHM()
{
    scp Rasp_*.sh $RASP:
    scp $SW_dir/Devices/ITs/Drivers/RaspCommons.sh $RASP:
    scp $SW_dir/Commons.sh $RASP:
    ssh $RASP "source Rasp_Bt_Ecd.sh;PrepareSessionEnv@SHM"

}

function GetReadyTheDischarge
{
  ssh $RASP "source Rasp_Bt_Ecd.sh;GetReadyTheDischarge"  
}

function SecurePostDischargeState()
{
      ssh $RASP "source Rasp_Bt_Ecd.sh;SecurePostDischargeState"
}
