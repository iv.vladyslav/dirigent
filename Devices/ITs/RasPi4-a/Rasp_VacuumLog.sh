#!/bin/bash

# To be executed at RASPs

source RaspCommons.sh

source Drivers/TPG262GNOME232/driver.sh
source Drivers/PapaGo2TC_ETH/driver.sh


# Device service for the GOLEM operation (kotvy/anchors) START
##############################################################


function PrepareSessionEnv@SHM()
{
    MountCentralSHMEnvironment 
}


function ReadChamberTemp()
{
    PapagoReadCh1
}



function PostDischargeFinals()
{

    #cat $SHML/GlobalLogbook|grep VacuumLog|awk '{print $1 " " $5 " " $7 }' >$SHM0/$SUBDIR/$ThisDev/Chamber.dat
    gnuplot  -e "set xdata time;set timefmt '%H:%M:%S';set xtics format '%tH:%tM' time;set xlabel 'Time [h:m]';set ylabel 'chamber pressure p_{ch} [mPa]';set xrange [*:*];set title 'Session chamber logbook';set yrange [*:50];set y2range [0:200];set y2tics 0, 20,200;set y2label'Chamber temperature T_{ch} [^o C]';set ytics nomirror;set terminal jpeg;plot '$SHM/ChamberLog' using 1:2 title 'pressure' with lines axis x1y1,'$SHM/ChamberLog' using 1:3 title 'temperature' with lines axis x1y2" > $SHM0/$SUBDIR/$ThisDev/SessionChamber_p_T_Logbook.jpg

       
    convert -resize $icon_size $SHM0/$SUBDIR/$ThisDev/SessionChamber_p_T_Logbook.jpg $SHM0/$SUBDIR/$ThisDev/graph.png
    convert /$SHM/Management/imgs/Chamber_icon.jpg $SHM0/$SUBDIR/$ThisDev/graph.png +append $SHM0/$SUBDIR/$ThisDev/icon_.png
    convert -bordercolor Black -border 2x2 $SHM0/$SUBDIR/$ThisDev/icon_.png $SHM0/$SUBDIR/$ThisDev/icon.png
    Web

}

  

function VacuumLog()
{
    echo -ne NULL > $SHML/GasSwitch;
    echo -ne 0 > $SHML/ActualVoltageAtGasValve;
    rm -f $SHM/ChamberLog 
    rm -f $SHML/GlobalLogbook

    while [ 1 ]; do
        for i in `seq 1 5`; do
            ActualChamberPressurePa=`get_chamber_pressure`
            ActualForVacuumPressurePa=`get_forvacuum_pressure`
            ActualChamberTemperature=`PapagoReadCh1`
            #echo $ActualChamberPressurePa > $SUBDIR/$ThisDev/ActualChamberPressurePa #NEJDE, ODSTRELUJE sshfs
            echo -ne $ActualChamberPressurePa > $SHML/ActualChamberPressurePa
            echo -ne  $ActualForVacuumPressurePa > $SHML/ActualForVacuumPressurePa
            echo -ne  $ActualChamberTemperature > $SHML/ActualChamberTemperature
            echo -ne  `echo $ActualChamberPressurePa*1000|bc|xargs printf '%4.2f'|sed 's/,/\./g'` > $SHML/ActualChamberPressuremPa
            echo `date '+%H:%M:%S'` "Chamber: `echo $ActualChamberPressurePa*1000|bc|xargs printf '%4.2f'` mPa, ForVacuum: $ActualForVacuumPressurePa Pa, Gas: `cat $SHML/GasSwitch`, GasValve: `cat $SHML/ActualVoltageAtGasValve|xargs printf '%4.2f'` V">> $SHML/PressureLog
            echo `date '+%H:%M:%S'` " " `echo $ActualChamberPressurePa*1000|bc|xargs printf '%4.2f'` " " `cat $SHML/ActualChamberTemperature|xargs printf '%4.2f'`  >> $SHM/ChamberLog
            #sleep 1 # neni potreba, je to zatim pomale dost
        done
        LogIt "Chamber: `echo $ActualChamberPressurePa*1000|bc|xargs printf '%4.2f'|sed 's/,/\./g'` mPa,  `cat $SHML/ActualChamberTemperature|xargs printf '%4.2f'` C, ForVacuum: $ActualForVacuumPressurePa Pa, Gas: `cat $SHML/GasSwitch`, GasValve: `cat $SHML/ActualVoltageAtGasValve|xargs printf '%4.2f'` V";
    done
}	



#Developing issues
#psa|grep VacuumLog|awk '{print $2}'|xargs kill
