#!/bin/bash

source /dev/shm/golem/Commons.sh

RASP=Chamber
SSHatRASP="ssh -Y golem@$RASP"
SOURCE='source Rasp_VacuumLog.sh;source Rasp_WorkingGas.sh'

# Device service for the GOLEM operation (kotvy/anchors) START
##############################################################

function PrepareSessionEnv@SHM()
{
    scp Rasp_*.sh $RASP:
    scp $SW_dir/Commons.sh $RASP:
    scp $SW_dir/Devices/ITs/Drivers/RaspCommons.sh $RASP:
    # $SSHatRASP "$SOURCE;PrepareSessionEnv@SHM" #maybe ..
    
    ssh chamber "source Rasp_VacuumLog.sh;PrepareSessionEnv@SHM"

}

function CallRasp() #maybe ..
{
    $SSHatRASP "$SOURCE;$1"
}

function VacuumLog()
{
    $SSHatRASP "$SOURCE;VacuumLog"
}

function VacuumLogzmb()
{
    ssh -Y golem@$RASP "source Rasp_VacuumLog.sh;source Rasp_WorkingGas.sh;VacuumLog"
}

function GetReadyTheDischarge()
{
    ssh -Y golem@$RASP "source Rasp_VacuumLog.sh;source Rasp_WorkingGas.sh;GetReadyTheDischarge"
}

function SecurePostDischargeState()
{
    ssh -Y golem@$RASP "source Rasp_VacuumLog.sh;source Rasp_WorkingGas.sh;SecurePostDischargeState"

}
