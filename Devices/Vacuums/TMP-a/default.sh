#!/bin/bash

Devices="RelayBoards/Quido16-a/VacuumControl"
. /dev/shm/golem/Commons.sh


function PingCheck(){ :; }

PUMP_id=a


function PumpingON
{
    LogIt "engaging TMP-$PUMP_id pump"
    CallFunction Devices/RelayBoards/Quido16-a/VacuumControl TMP-aON
}

function PumpingOFF
{
    LogIt "disengaging TMP-$PUMP_id pump"
    CallFunction Devices/RelayBoards/Quido16-a/VacuumControl TMP-aOFF
}

function StandbyON
{
    LogIt "Stand by TMP-$PUMP_id pump"
    CallFunction Devices/RelayBoards/Quido16-a/VacuumControl TMP-aStandbyON
}

function StandbyOFF
{
    LogIt "Stand by TMP-$PUMP_id pump OFF"
    CallFunction Devices/RelayBoards/Quido16-a/VacuumControl TMP-aStandbyOFF
}
