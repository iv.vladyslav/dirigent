#!/bin/bash

Devices="Devices/RelayBoards/Quido8-a/Switchboard-I"
. /dev/shm/golem/Commons.sh


function PingCheck(){ :; }

function PumpingON
{
    LogIt "engaging rotary pump"
    CallFunction Devices/RelayBoards/Quido8-a/Switchboard-I RotPumpON
}

function PumpingOFF
{
    LogIt "disengaging rotary pump"
    CallFunction Devices/RelayBoards/Quido8-a/Switchboard-I RotPumpOFF
}

