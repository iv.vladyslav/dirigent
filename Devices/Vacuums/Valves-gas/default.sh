#!/bin/bash

Devices="RelayBoards/Quido16-a/VacuumControl"
. /dev/shm/golem/Commons.sh


function PingCheck(){ :; }

function GasH2ON
{
    LogIt "GasH2ON"
    CallFunction Devices/RelayBoards/Quido16-a/VacuumControl GasH2ON
}

function GasH2OFF
{
    LogIt "GasH2OFF"
    CallFunction Devices/RelayBoards/Quido16-a/VacuumControl GasH2OFF
}

function GasHeON
{
    LogIt "GasHeON"
    CallFunction Devices/RelayBoards/Quido16-a/VacuumControl GasHeON
}

function GasHeOFF
{
    LogIt "GasHeOFF"
    CallFunction Devices/RelayBoards/Quido16-a/VacuumControl GasHeOFF
}

function GasFlowSetup ()
{
Voltage=$1
    CallFunction Devices/PowerSupplies/GWInstekPSW-a/Working_Gas GasFlowSetup $Voltage
}  


