#!/bin/bash

Devices="RelayBoards/Quido16-a/VacuumControl"
. /dev/shm/golem/Commons.sh

PUMP_id=b

function PingCheck(){ :; }

function PumpingON
{
    LogIt "engaging TMP-$PUMP_id pump"
    CallFunction Devices/RelayBoards/Quido16-a/VacuumControl TMP-bON
}

function PumpingOFF
{
    LogIt "disengaging TMP-$PUMP_id pump"
    CallFunction Devices/RelayBoards/Quido16-a/VacuumControl TMP-bOFF
}

function StandbyON
{
    LogIt "Stand by TMP-$PUMP_id pump"
    CallFunction Devices/RelayBoards/Quido16-a/VacuumControl TMP-bStandbyON
}

function StandbyOFF
{
    LogIt "Stand by TMP-$PUMP_id pump OFF"
    CallFunction Devices/RelayBoards/Quido16-a/VacuumControl TMP-bStandbyOFF
}
