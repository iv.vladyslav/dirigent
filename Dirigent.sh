#!/bin/bash

SHM="/dev/shm/golem"
#SW_dir=`pwd`;export SW_dir

function Prerequisities()
{
    mkdir -p $SHM
    hostname > $SHM/hostname # migration to SuperMicro
    cp Dirigent.sh Commons.sh $SHM/
    #if [ -e /dev/shm/golem/session.setup ]; 
    #then 
        # setup aktualizace ...
        cp current.setup $SHM/session.setup
        cp `realpath current.setup` $SHM/
    #fi
        echo `realpath current.setup |xargs basename -s .setup` > $SHM/session_setup_name
        


    source $SHM/Commons.sh
    echo $((`CurrentShotDataBaseQuerry "shot_no"`)) > $SHM/shot_no 
}

Prerequisities


# SaveCommandLineParams()
    if [[ $1 == "--discharge" ]]; then 
    mkdir -p $SHMCLP
    rm $SHMCLP/*
    cp Dirigent.sh Commons.sh $SHM0/

    args=("$@")
    
    CL='./Dirigent.sh --discharge'
    #CL='./Dirigent.sh'

    while [ $# -gt 0 ]; do
   if [[ $1 == *"--"* ]]; then
        v="${1/--/}"
        declare $v="$2"
        echo $2 > $SHMCLP/$v
 #       echo $v=$2
           if [ $v == "comment" ] || [[ "$v" =~ "diagnostics." ]] || [[ "$v" =~ "discharge." ]] || [ $v == "ScanDefinition" ]; then
            CL="$CL --$v \"$2\"";
         elif [ $v == "discharge" ]; then
         echo ;
         else
          
          
          
          CL="$CL --$v $2"
        fi
   fi
  shift
done
echo "$CL" > $SHMCLP/CommandLine

set -- $args
fi







function SandBox()
{
######### Test part #############
        rm -rf  $SHM0
        Broadcast Everywhere PrepareDischargeEnv@SHM "$@" # Get ready the whole 
        exit
########### End test part ###################
}


#Main command interaction with user, here it starts
#============================================

TASK=$1
#COMMANDLINE=`echo $@|sed 's/-r //g'`
# ToDo replace special chars: echo '!@#[]$%^&*()-' | sed 's/[]!@#[$%^&*()-]/\\&/g' ..... \!\@\#\[\]\$\%\^\&\*\(\)\-


case "$TASK" in
   "") 
      echo "Usage: $0 [-d|--discharge] [-o|--open] [-t|--test]  [-s|--shutdown]"
      RETVAL=1
      ;;
      --ping)
      Broadcast Everywhere PingCheck
      ;;
      --wake)
      Broadcast Everywhere WakeOnLan
      ;;
      --sleep)
      Broadcast Everywhere SleepOnLan
      ;;
      --reset)
      KillAllGMtasksEverywhere
      pkill -f json_status4remote.py
      rm -rf /dev/shm/golem;mkdir -p /dev/shm/golem
     ;;
      --resetup)
      ssh -Y golem@Chamber.golem  "killall -u golem"
      ssh -Y golem@golem "killall xterm"
      rm -rf /dev/shm/golem;
      echo "Jeste sestrel rucne feedgnuplot"
      ;;
      --open)
      source current.setup # source requested setup Setups/XXYY.setup
      cp current.setup $SHM/session.setup
      cp `realpath current.setup` $SHM/
      source Commons.sh
      mkdir -p $SHMS;
      #rsyncRASPs
      Broadcast Everywhere PrepareSessionEnv@SHM $DirigentServer # Get ready the whole dir struct @SHMS
      cd $SHMS;
      Broadcast Everywhere OpenSession
      SubmitTokamakState "idle" 
      tail -f $SHML/GlobalLogbook
      ;;
      --discharge|-d|--d)
      #MakeDischarge  "$@"
      CallFunction Operation/Discharge/DischargeManagement Discharge
      ;;
      --shutdown|-s)
        Broadcast Everywhere SleepOnLan
        KillAllGMtasksEverywhere
        pkill -f json_status4remote.py
        rm -rf $SHM;
      ;;
      --pon)
      xterm -fg yellow -bg blue -title "Golem pumping start" -e "source Commons.sh;CallFunction Operation/Session/ChamberManagement PumpingON"
      ;;
      --poff)
      xterm -fg yellow -bg blue -title "Golem pumping end" -e "source Commons.sh;CallFunction Operation/Session/ChamberManagement PumpingOFF"
      ;;
      --bon) #baking ON
      xterm -fg yellow -bg blue -title "Baking status" -hold -e "source $SW_dir/Operation/Session/ChamberManagement.sh; Baking_ON 200 8" #Final Temperature Pressure
      ;;
      --boff) #baking OFF
      CallFunction Operation/Session/ChamberManagement Baking_OFF
      ;;
      --gdon) #Glow discharge ON
      xterm -fg yellow -bg blue -title "Glow disch management" -e "echo Dggdi: Glow discharge initiate;bash"
      ;;
      --gdi) #Glow discharge init
      CallFunction Operation/Session/ChamberManagement GlowDischInitiate
      ;;
      --gds) #Glow discharge init
      CallFunction Operation/Session/ChamberManagement GlowDischStop
      ;;
      --gdfs) 
      Voltage=$2
      CallFunction Operation/Session/ChamberManagement GlowDischGasFlowSetup $Voltage
      ;;
      --gdw) 
      Time=$2 #[min]
      CallFunction Operation/Session/ChamberManagement GlowDischWaitForStop $Time
      ;;
      --fc)
      WGcalH2
      ;;
      --sandbox|--snb)
      rm -rf  $SHM0
      Broadcast Everywhere PrepareDischargeEnv@SHM "$@" # Get ready the whole
      ;;
    --emergency|-e|--e)
      Broadcast Everywhere SecurePostDischargeState
      ;; 
      --reset|-r)
      KillAllGMtasksEverywhere
      rm -rf /dev/shm/golem;
       ;; 
      --tt) #basic trigger test
      CallFunction Operation/Discharge/DischargeManagement TriggerRequest TriggerTest
      ;;
      --rs) 
      source Commons.sh ;rsyncRASPs 
      ;;
      --backup) #backup
      $psql_password;pg_dump golem_database > golem_database.sql; 
      rsync -r -u -v -K -e ssh --exclude '.git' $PWD svoboda@bn:backup/Dirigent/`date "+%y%m%d"`
      zip golem_database golem_database.sql;mpack -s "GM database `date`" golem_database.zip tokamakgolem@gmail.com;
      rm golem_database.*
      ;;
      --dummy|--dd)
      ./Dirigent.sh --discharge --UBt 0 --TBt 1000 --Ucd 0 --Tcd 2000 --preionization 1 --gas H --pressure 10  --Bt_orientation "CW" --CD_orientation "CW"  --comment "Dummy test discharge"
      ;;
      --modest|--dm)
      ./Dirigent.sh --discharge --UBt 220 --TBt 1000 --Ucd 220 --Tcd 2000 --preionization 1 --gas H --pressure 10 --Bt_orientation "CW" --CD_orientation "CW"   --comment "Modest test discharge"
      ;;
      --standard|--ds)
      ./Dirigent.sh --discharge --UBt 800 --TBt 1000 --Ucd 450 --Tcd 2000 --preionization 1 --gas H --pressure 10  --Bt_orientation "CW" --CD_orientation "CW"   --comment "Standard test discharge"
      ;;
      --sujb|--dds)
      ./Dirigent.sh --discharge --UBt 800 --Ucd 400 --Tcd 1000  --Tres 2000 --pressure 20  --Bt_orientation "CW" --CD_orientation "CW"   --comment "Standardni test dlouhodobe stability"
      ;;
      --help|-h|--h)
      echo "**********Fequently used commands************"
      alias

      ;;
esac


#Tuning ...
