#!/bin/bash

source ../../../Commons.sh

whoami="Operation/Discharge/Position_Stabilization/Stabilization"

diags=('radial' 'vertical')   
#DAS="Oscilloscopes/RigolMSO5204-e/Stabilization Oscilloscopes/RigolMSO5104-a/Stabilization"
DAS="Oscilloscopes/RigolMSO5204-e/Stabilization Oscilloscopes/RigolMSO5204-b/Stabilization"

FG="RigolDG1022Z-a"

Devices="FunctionGenerators/$FG/Stabilization Amplifiers/KepcoStabilizationRack/Basic $DAS"


function WakeOnLan(){ 
    sleep 1
    echo "*B1OS1H"|telnet 192.168.2.254 10001 1>&- 2>&-; # Zasuvky Jižní stěna: FG a OSC
    sleep 1


} 

function SleepOnLan(){ 
    echo "*B1OS1L"|telnet 192.168.2.254 10001 1>&- 2>&-; # Zasuvky Jižní stěna: FG a OSC
}


function DefineTable()
{
    CreateTable discharge.position_stabilization
}


function OpenSession()
{

    InitFunctGen 
}

StabilizationON () {
ssh Charger "source Bt_Ecd.sh ;UpperShortCircuitsEngage"
#sleep 1;ssh golem@Discharge.golem "source Trigger.sh;DummyTrigger" # Fast cameras needs dummy triger /for now/
#ssh golem@Stabilization 'echo "import lib as lib;lib.gpio_low(16)"|python'; 
}
StabilizationOFF () {
ssh Charger "source Bt_Ecd.sh ;UpperShortCircuitsDisEngage"
#ssh golem@Stabilization 'echo "import lib as lib;lib.gpio_high(16)"|python'; 
}

InitFunctGen ()
{
for channel in 1 2; do 
    echo ":OUTP$channel OFF"|netcat -w 1 $FG 5555 #Turn off output of channel
    sleep 0.1;
    echo "Burst mode"
    echo ":SOUR$channel:BURS ON"|netcat -w 1 $FG 5555
    echo ":SOUR$channel:BURS:MODE:TRIG;:SOUR$channel:BURS:TRIG:SOUR EXT;:SOUR$channel:BURS:NCYC 1 "|netcat -w 1 $FG 5555 #Set trigger to manual/external and number of 
done

}


FunctGen () {
raw=$1
channel=$2 
echo 'channel:'$channel
raw=$(echo $raw | tr -s ',' ' ')
raw=$(echo $raw | tr -s ';' ' ')

time=()
value=()
a=0
for i in $raw
    do
      if [[ a%2 -eq 0 ]]    
      then
        time+=($i)
      else
        value+=($i)
      fi
    let "a+=1"
    done

echo Time points: ${time[@]}
echo Values: ${value[@]}

sign () { echo "$(( $1 < 0 ? -1 : 1 ))"; } #absolute value

#looks for the max value
Max=0
for m in ${value[@]} 
    do
      m=$(( $m * $(sign "$m") ))    
      if [[ $m -gt $Max ]]
      then 
         Max=$m 
      fi
    done
echo Max value: $Max

#generate sequence of points for the frequency generator
sequence=()
j=0
l=1
for ((i=${time[0]};i<=${time[-1]};i+=100))
   do
     k=${value[$l-1]}
     sequence+=$(python -c "print(round(${k}.0 / ${Max}.0,1),',')") #strange but functional
     if [[ $i -eq ${time[$l]} ]]
     then
        let "l+=1"
     fi
    let "j+=1"
   	done

c=${sequence::-2} #remove last two characters ( ,) 
c=$(echo $c | tr -s ' ,' ',') #replace " ," with ","
echo Final sequence: $c

echo ":OUTP$channel OFF"|netcat -w 1 $FG 5555 #Turn off output of channel
sleep 0.1;
echo "Waveform pars"
echo ":SOUR$channel:APPL:ARB 10000,$Max,0"|netcat -w 1 $FG 5555 #Arbitrary 
echo ":SOUR$channel:DATA VOLATILE, $c"|netcat -w 1 $FG 5555 #predefined function
# Zkusime pres InitFreqGen
#echo "Burst mode"
echo ":SOUR$channel:BURS ON"|netcat -w 1 $FG 5555
#echo ":SOUR$channel:BURS:MODE:TRIG;:SOUR$channel:BURS:TRIG:SOUR EXT;:SOUR$channel:BURS:NCYC 1 "|netcat -w 1 $FG 5555 #Set trigger to manual/external and number of 
sleep 0.2;
echo ":SOUR$channel:BURS:TDEL 0.00${time[0]}"|netcat -w 1 $FG 5555 #Set time delay (in seconds)
}   

FG_channel_radial=1
FG_channel_vertical=2


function GetReadyTheDischarge ()
   {
    mkdir Parameters
    GeneralTableUpdateAtDischargeBeginning "discharge.position_stabilization"
    
    if [ `cat $SHM0/Operation/Discharge/Position_Stabilization/Parameters/main_switch` == "on" ] ; then
    StabilizationON
    for i in radial vertical; do
       if [ `cat $SHM0/Operation/Discharge/Position_Stabilization/Parameters/"$i"_switch` == "on" ] ; then
        act_channel=$( eval "echo \${FG_channel_$i}")
        act_setup=`cat $SHM0/Operation/Discharge/Position_Stabilization/Parameters/"$i"_waveform`
        echo $act_setup
        FunctGen  $act_setup $act_channel
      fi
    done
    fi
 }
 

function Arming()
{
       if [ `cat $SHM0/Operation/Discharge/Position_Stabilization/Parameters/main_switch` == "on" ] ; then
            for i in radial vertical; do
                if [ `cat $SHM0/Operation/Discharge/Position_Stabilization/Parameters/"$i"_switch` == "on" ] ; then
                    act_channel=$( eval "echo \${FG_channel_$i}")
                    echo ":OUTP$act_channel ON"|netcat -w 1 $FG 5555 
                else
                    echo ":OUTP$act_channel OFF"|netcat -w 1 $FG 5555 
                fi
            done
        else 
            echo Stabilization main switch OFF
        fi
}
   
   
   function PostDischargeAnalysis
{

local cesta=`dirname $whoami`

    echo ":OUTP2 OFF"|netcat -w 1 $FG 5555 #Turn off output of ch2
    echo ":OUTP1 OFF"|netcat -w 1 $FG 5555 #Turn off output of ch1
    StabilizationOFF
    
   
 
echo "
<caption>`cat $SHM0/Production/Parameters/discharge.position_stabilization`</caption>
<tr>
    <td valign=bottom><a href=$cesta/><img src=$cesta/name.png  width='$namesize'/></a></td>" > include.html
    for i in 1 2; do
       CurrDAS=`echo $DAS|awk "{print $"$i"}"` 
       echo doing $CurrDAS
       GeneralDAScommunication $CurrDAS RawDataAcquiring
       cp ../../../Devices/`dirname $CurrDAS`/*.csv .
       cp ../../../Devices/`dirname $CurrDAS`/ScreenShotAll.png ScreenShot${diags[$i-1]}.png
       ln -s ../../../Devices/`dirname $CurrDAS/` DAS_raw_data_dir_${diags[$i-1]}
       echo "<td valign=bottom><a href=$cesta/${diags[$i-1]}_setup.png><img src=$cesta/${diags[$i-1]}_setup.png width='$iconsize'/></a></td><td valign=bottom><a href=$cesta/ScreenShot${diags[$i-1]}.png><img src='$cesta/ScreenShot${diags[$i-1]}.png'  width='$iconsize'/></a></td>" >> include.html
    done
    
    echo "<td valign=bottom><a href=$cesta/analysis.html><img src=$cesta/graph.png  width='$iconsize'/></a></td>
    <td>
    <a href=$cesta/Parameters/ title="Parameters">$parametersicon</a><br></br>
    <a href=$gitlabpath/$whoami.ipynb title="JupyterNotebook$cesta">$pythonicon</a>
    $rightarrowicon
    <a href=$cesta/analysis.html title="Analysis$cesta">$resultsicon</a><br></br>
    <a href=$gitlabpath/$cesta/ title="Gitlab4$cesta">$gitlabicon</a>
    <a href=$cesta/ title="Directory">$diricon</a>
    <a href=$dbpath/Diagnostics/$diag_id/ title="Database">$sqlicon</a>
    </td>

    </tr>" >> include.html
    Analysis
      
}      
      
function Analysis
{

    mkdir Results
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat ../../../shot_no`/g" `basename $whoami`.ipynb
    jupyter-nbconvert  --ExecutePreprocessor.timeout=60  --to html --execute `basename $whoami`.ipynb --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2) #Lokalni vypocet
    convert -resize $icon_size icon-fig.png graph.png
    
}


#shot_no=35993;What=homepage_row;Where="Operation/Discharge/Stabilization/Stabilization.sh"; cp -r /golem/svoboda/Dirigent/`dirname $Where`/*.* /golem/database/operation/shots/$shot_no/`dirname $Where`/;cd /golem/database/operation/shots/$shot_no/`dirname $Where`;source `basename $Where` ;$What;


