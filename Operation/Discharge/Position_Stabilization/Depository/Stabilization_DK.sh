#!/bin/bash

source ../../../Commons.sh

whoami="Operation/Discharge/Stabilization/Stabilization"

DAS="Oscilloscopes/RigolMSO5104-a/Stabilization"


Devices="FunctionGenerators/RigolDG1032Z-a/FG4Stabilization RelayBoards/Energenie_LANpower-b/AdHocPowerSupply RASPs/Stabilization/Stabilization $DAS"


FlukeAmpMeterON () {
ssh golem@Stabilization 'echo "import lib as lib;lib.gpio_low(16)"|python'; }
FlukeAmpMeterOFF () {
ssh golem@Stabilization 'echo "import lib as lib;lib.gpio_high(16)"|python'; }


 function GetReadyTheDischarge ()
{
    FlukeAmpMeterON
    GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh
}


function Arming()
{
    raw=`cat $SHM0/Production/Parameters/diagnostics_stabilization`
    
    raw=$(echo $raw | tr -s ',' ' ')
raw=$(echo $raw | tr -s ';' ' ')

time=()
value=()
a=0
for i in $raw
    do
      if [[ a%2 -eq 0 ]]    
      then
        time+=($i)
      else
        value+=($i)
      fi
    let "a+=1"
    done

echo Time points: ${time[@]}
echo Values: ${value[@]}


sign () { echo "$(( $1 < 0 ? -1 : 1 ))"; } #absolute value

#looks for the max value
Max=0
for m in ${value[@]} 
    do
      m=$(( $m * $(sign "$m") ))    
      if [[ $m -gt $Max ]]
      then 
         Max=$m 
      fi
    done
echo Max value: $Max

#generate sequence of points for the frequency generator
sequence=()
j=0
l=1
for ((i=${time[0]};i<=${time[-1]};i+=100))
   do
     k=${value[$l-1]}
     sequence+=$(python -c "print(round(${k}.0 / ${Max}.0,1),',')") #strange but functional
     if [[ $i -eq ${time[$l]} ]]
     then
        let "l+=1"
     fi
    let "j+=1"
   	done
c=${sequence[@]}
echo Final sequence: $c


#========Frequency generator settings===========

##echo "*IDN?"|netcat -w 1 192.168.2.171 5555
echo ":OUTP2 OFF"|netcat -w 1 192.168.2.171 5555 #Turn off output off ch2
sleep 0.1;
#First, set the waveform parameters
echo ":SOUR2:APPL:ARB 10000,$Max,0"|netcat -w 1 192.168.2.171 5555 #Arbitrary waveform, Samples rate:10000Sa/s (1point = 100us); amplitude [V], offset:0V 
echo ":SOUR2:DATA VOLATILE, $c"|netcat -w 1 192.168.2.171 5555 #predefined function

#Now set the burst mode
echo ":SOUR2:BURS ON"|netcat -w 1 192.168.2.171 5555
echo ":SOUR2:BURS:MODE:TRIG;:SOUR2:BURS:TRIG:SOUR EXT;:SOUR2:BURS:NCYC 1 "|netcat -w 1 192.168.2.171 5555 #Set trigger to manual/external and number of cycle to ...
sleep 0.2;

#echo ":SOUR2:BURS:TRIG:SLOP POS"|netcat -w 1 192.168.2.171 5555 #Set CH2 to type of trigger input on the falling edge (use with external trigger)

echo ":SOUR2:BURS:TDEL 0.00${time[0]}"|netcat -w 1 192.168.2.171 5555 #Set time delay (in seconds)

echo ":OUTP2 ON"|netcat -w 1 192.168.2.171 5555 #Turn on output of ch2

#echo ":SOUR2:BURS:TRIG"|netcat -w 1 192.168.2.171 5555 #Manual trigger
    
}
   
   
   function PostDischargeAnalysis
{
    FlukeAmpMeterOFF
    GeneralDAScommunication $DAS RawDataAcquiring
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir

    Analysis

      
}      
      
function Analysis
{

#Workarround
    echo "ahoj" > include.html
#    convert -resize $icon_size icon-fig.png graph.png
}




