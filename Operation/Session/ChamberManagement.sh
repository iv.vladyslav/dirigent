#!/bin/bash

source /dev/shm/golem/Commons.sh

Devices="Vacuums/RotaryPump/default ITs/RasPi4-a/Chamber Interfaces/PapaGo2TC_ETH-a/ChamberTemperature Interfaces/Gnome232-a/VacuumGauge RelayBoards/Quido16-a/VacuumControl"


# Device functions
###################




# Device service for the GOLEM operation (kotvy/anchors) START
##############################################################



function OpenSession()
{
    CallFunction Devices/ITs/RasPi4-a/Chamber VacuumLog  
    sleep 2
    UpdateCurrentSessionDataBase "start_chamber_pressure=`cat $SHML/ActualChamberPressurePa`,start_for_vacuum_pressure=`cat $SHML/ActualForVacuumPressurePa`"
    
}

function SecurePostDischargeState()
{
    $LogFunctionGoingThrough
    echo "Discharge end" >>$SHML/PressureLog
    #cp $SHML/ActualChamberPressuremPa $SHM0/$SUBDIR/$ThisDev/final_chamber_pressure
    #echo 25> $SHM0/$SUBDIR/$ThisDev/final_chamber_temperature
    UpdateCurrentShotDataBase "p_chamber_pressure_after_discharge=`cat $SHML/ActualChamberPressuremPa`"
}

# Device service for the GOLEM operation (kotvy/anchors) END
##############################################################


function xtermPumpingON(){
    xterm -fg yellow -bg blue -title "Golem pumping start" -hold -e /bin/bash -l -c "PumpingON"
    }

function PumpingON(){
    if [ -e $SHMS/session_date ]; then
        $LogFunctionStart
        $psql_password;psql -c "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('pumping:start','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
        CallFunction Devices/Vacuums/RotaryPump/default PumpingON
        LogIt "sleep 2 s to run TMPs ...";sleep 2
        CallFunction Devices/Vacuums/TMP-a/default PumpingON
        CallFunction Devices/Vacuums/TMP-b/default PumpingON
        LogIt "sleep 20 s to open all valves ...";sleep 20
        CallFunction Devices/Vacuums/Valves-a/default OpenValves
        CallFunction Devices/Vacuums/Valves-b/default OpenValves
        $LogFunctionEnd
    else
        critical_error "Please, first open a session"
    fi
}


function xtermPumpingOFF(){
    xterm -fg yellow -bg blue -title "Golem pumping end" -hold -e /bin/bash -l -c "PumpingOFF"
    }


function PumpingOFF()	
{ 
    $LogFunctionStart
    $psql_password;psql -c "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('pumping:end','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
    Relax
    #echo "UPDATE chamber_pumping SET end_time='`date +%H:%M:%S`',end_pressure=`cat $SHML/ActualChamberPressuremPa`,end_temperature=`cat $SHML/ActualChamberTemperature`  WHERE id IN(SELECT max(id) FROM chamber_pumping)"|ssh Dg "cat - |psql -q -U golem golem_database"
    LogIt "Closing Valves and disengaging TMPs"
    CallFunction Devices/Vacuums/Valves-a/default CloseValves
    CallFunction Devices/Vacuums/Valves-b/default CloseValves
    CallFunction Devices/Vacuums/TMP-a/default PumpingOFF
    CallFunction Devices/Vacuums/TMP-b/default PumpingOFF
    LogIt "sleep 5 s to stop Rotary pump ...";sleep 5
    CallFunction Devices/Vacuums/RotaryPump/default PumpingOFF
    $LogFunctionEnd
}



function GlowDischInitiate()
{
    $LogFunctionPassing;
    CallFunction Devices/Vacuums/TMP-a/default StandbyON
    CallFunction Devices/Vacuums/TMP-b/default StandbyON
    CallFunction Devices/PowerSupplies/GWInstekGPR-a/GlowDischarge PowerON
    CallFunction Devices/Vacuums/Valves-a/default CloseValves
    CallFunction Devices/Vacuums/Valves-gas/default GasHeON
    $psql_password;psql -c "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('glow discharge:init','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
    echo "!!ENGAGE HV PS & Open gas reservoir !!"
    echo './Dirigent.sh --gdfs 70/50 # Set-up flow'
    echo './Dirigent.sh --gdw 15 # Wait for stop X mins'
    echo './Dirigent.sh --gds # Stop it'
    
}    

function GlowDischGasFlowSetup ()
{
    Voltage=$1
    CallFunction Devices/Vacuums/Valves-gas/default GasFlowSetup $Voltage
}    
    
    

function GlowDischWaitForStop()
{
Time=$1
    $psql_password;psql -c "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('glow discharge:start','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
    for i in `seq 1 $Time`; do echo Waiting for GD to stop $i/$Time; sleep 1m;done
    GlowDischStop
    
}    

function GlowDischStop()
{
    $LogFunctionPassing;
    GlowDischGasFlowSetup 0
    sleep 10s
    CallFunction Devices/PowerSupplies/GWInstekGPR-a/GlowDischarge PowerOFF
    CallFunction Devices/Vacuums/TMP-a/default StandbyOFF
    CallFunction Devices/Vacuums/TMP-b/default StandbyOFF
    sleep 5s
    CallFunction Devices/Vacuums/Valves-a/default OpenValves
    CallFunction Devices/Vacuums/Valves-gas/default GasHeOFF
    echo "!!Close He reservoir !!"
    $psql_password;psql -c "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('glow discharge:end','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
}  

function Baking_ON(){
DestinationTemp=$1
DestinationPressure=$2

    $LogFunctionPassing;
     $psql_password;psql -c "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('baking:start','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
     
    CallFunction Devices/RelayBoards/ABBswitch-c/Et_commutator OFF
     sleep 2
    ActualChamberPressuremPa=`cat $SHML/ActualChamberPressuremPa`
    ActualChamberTemperature=`cat $SHML/ActualChamberTemperature`

    CallFunction Devices/PowerSupplies/PowerGrid/Baking SwitchON
    while (( $(bc <<< "$ActualChamberPressuremPa < $DestinationPressure") && $(bc <<< "$ActualChamberTemperature < $DestinationTemp") ));  do 
    for i in `seq 10`;do  read -r ActualChamberPressuremPa < $SHML/ActualChamberPressuremPa;if [ -z $ActualChamberPressuremPa ];then echo Problem;else break;fi; sleep 0.1;done;
        #read -r ActualChamberPressuremPa < $SHML/ActualChamberPressuremPa
    for i in `seq 10`;do  read -r ActualChamberTemperature < $SHML/ActualChamberTemperature;if [ -z $ActualChamberTemperature ];then echo Problem;else break;fi; sleep 0.1;done;
        #read -r ActualChamberTemperature < $SHML/ActualChamberTemperature
    echo `date "+%H:%M:%S"` : $ActualChamberPressuremPa/$DestinationPressure or $ActualChamberTemperature/$DestinationTemp
        sleep 1
    done
    CallFunction Devices/PowerSupplies/PowerGrid/Baking SwitchOFF
    
}

function Baking_OFF(){
    $LogFunctionPassing;
    CallFunction Devices/PowerSupplies/PowerGrid/Baking SwitchOFF
    $psql_password;psql -c "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('baking:end','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
    #echo "UPDATE chamber_baking SET end_time='`date +%H:%M:%S`',end_pressure=`cat $SHML/ActualChamberPressuremPa`,end_temperature=`cat $SHML/ActualChamberTemperature`  WHERE id IN(SELECT max(id) FROM chamber_baking)"|ssh Dg "cat - |psql -q -U golem golem_database"
}

function Baking_test()
{
    $LogFunctionStart
    Baking_ON
    sleep 1s
    Baking_OFF
    $LogFunctionEnd

}


function ReadChamberTemp()
{
    PapagoReadCh1
}

#Developing issues
####################
#cp Dirigent.sh Commons.sh /golem/shm_golem/ActualSession/;./Dirigent.sh --pon
