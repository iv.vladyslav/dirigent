#!/bin/bash
SHM="/dev/shm/golem"
mkdir -p $SHM
cp Dirigent.sh Commons.sh $SHM/

source /dev/shm/golem/Commons.sh

xtermpars='-fn "-misc-fixed-medium-r-normal--20-*-*-*-*-*-iso8859-15" +sb -geometry 150x50+620+2 -fg yellow -bg blue '
drigentcommand='cd /golem/Dirigent/;echo Welcome > '$SHM'/nohup.out;nohup bash Dirigent.sh >> $SHM/nohup.out'

whoami="Operation/Session/Controlling"

Devices=""

Chamber="ssh -Y golem@Chamber bash --login -c"
Charger="ssh golem@Charger"



function BasicManagementCore ()
{
echo 'option add *font "Helvetica 15";\
wm title . Dirigent@Operator
frame .session;
set setup current.setup;\
set SHM /dev/shm/golem;\
label .syst -text {Session: broadcast} -background blue;\
label .setuplabel -text {Setup:} -background white;\
label .l -text "current.setup";\
button .wakeonlan -text "wake" -command {exec xterm -hold -title "Wake on lan" '$xtermpars' -e "'$drigentcommand' --setup $setup  w & tail -f $SHM/nohup.out"};\
button .pingcheck -text "ping" -command {exec xterm -title "Ping Check" '$xtermpars' -e "'$drigentcommand' --setup $setup  p & tail -f $SHM/nohup.out"};\
button .opensession -text "simple open" -command {exec {*}xterm -title "Open session" '$xtermpars' -e "'$drigentcommand' --setup $setup  o & tail -f $SHM/nohup.out" &};\
button .resetsession -text "reset" -command {exec xterm -title "Reset session" '$xtermpars' -e "'$drigentcommand' --setup $setup  reset & tail -f $SHM/nohup.out" };\
button .rsyncall -text "rsync" -command {exec xterm -title "BC Rsync all" '$xtermpars' -e "'$drigentcommand' -b rs & tail -f $SHM/nohup.out" };\
button .bopensession -text "os" -command {exec xterm -title "BC: Open session" '$xtermpars' -e "'$drigentcommand' -b os & tail -f $SHM/nohup.out" };\
pack  .syst .setuplabel .l   .pingcheck .opensession -in .session -side left;pack .session;\
button .shutdownsession -text "shutdown" -command {exec xterm -title "Shutdown session" '$xtermpars' -e "'$drigentcommand' -s & tail -f $SHM/nohup.out" };\
button .emergency -text "emergency" -command {exec xterm -title "Emergency" '$xtermpars' -e "'$drigentcommand' -e & tail -f $SHM/nohup.out" } -background red;\
button .sleeponlan -text "sleep" -command {exec xterm -title "Sleep on line devices" '$xtermpars' -e "'$drigentcommand' --setup $setup b SleepOnLan & tail -f $SHM/nohup.out" };\
pack  .syst .setuplabel .l   .wakeonlan  .pingcheck .opensession .resetsession .rsyncall .bopensession .sleeponlan .emergency .shutdownsession -in .session -side left;pack .session;\
;\
frame .vacuum;\
label .lab_vacuum -text {Vacuum:} -background blue;\
button .v_on -text "Pumping START" -command {exec bash -c "'$drigentcommand' --pon"} -background cyan;\
button .v_off -text "Pumping END" -command {exec bash -c "'$drigentcommand' --poff"} -background cyan;\
pack .lab_vacuum .v_on .v_off -in .vacuum -side left;\
pack .vacuum;\
;\
frame .chamber_cond;\
label .lab_chamber_cond -text {Chamber conditioning:} -background blue;\
button .etoff -text "Et off" -command {exec '$Charger' "source Charger.sh;EtCommutatorOFF" 2>/dev/null}  -background purple;\
button .bak_on -text "Baking ON" -command {exec '$Chamber' Baking_ON} -background purple;\
button .bak_off -text "Baking OFF" -command {exec '$Chamber' Baking_OFF} -background purple;\
pack .lab_chamber_cond .etoff .bak_on .bak_off -in .chamber_cond -side left;\
pack .chamber_cond;\
;\
frame .wg;\
label .lab_wg -text {Working gas:} -background blue;\
button .wg_cal_h2 -text "Fake calibration" -command {exec xterm -title "Fake calibration" '$xtermpars' -e "'$drigentcommand' -r WGcalH2 & tail -f $SHM/nohup.out"} -background green;\
button .wg_test_h2 -text "WG H2 test " -command {exec '$Chamber' WGtestH2};\
pack .lab_wg .wg_cal_h2 .wg_test_h2 -in .wg -side left;\
pack .wg;\
;\
frame .discharge;\
label .lab_discharge -text {Discharge:} -background blue;\
button .d_dummy -text "dummy" -command {exec xterm -title "Dummy discharge" '$xtermpars' -e "'$drigentcommand' --dummy & tail -f $SHM/nohup.out"} -background green;\
button .d_modest -text "modest" -command {exec xterm -title "Modest discharge" '$xtermpars' -e "'$drigentcommand' --modest & tail -f $SHM/nohup.out"} -background orange;\
button .d_standard -text "standard" -command {exec xterm -title "Standard discharge" '$xtermpars' -e "'$drigentcommand' --standard & tail -f $SHM/nohup.out"}  -background red;\
button .d_sujb -text "sujb" -command {exec xterm -title "Standardní test dlouhodobé stability" '$xtermpars' -e "'$drigentcommand' --sujb & tail -f $SHM/nohup.out"}  -background red;\
pack .lab_discharge .d_dummy .d_modest .d_sujb .d_standard  -in .discharge -side left;\
pack .discharge;\
frame .production;\
label .lab_edit -text {Production:} -background blue;\
button .prod_nano_this -text "nano this" -command {exec xterm -title "Edit this" '$xtermpars' -e "nano /golem/Dirigent/Operation/Session/Controlling.sh"} -background green;\
button .prod_kate_this -text "Kate this" -command {exec kate /golem/Dirigent/Operation/Session/Controlling.sh} -background green;\
pack .lab_edit .prod_nano_this .prod_kate_this -in .production -side left;\
pack .production;\
'|wish
}
