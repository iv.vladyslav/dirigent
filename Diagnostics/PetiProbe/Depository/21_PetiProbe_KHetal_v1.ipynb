{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Electron temperature measurement with the ball-pen and Langmuir probe\n",
    "\n",
    "The combined probe head (ball-pen probe + Langmuir probe) offers measurements of the plasma potential $\\Phi$, floating potential $V_{fl}$ and electron temperature $T_e$ with high temporal resolution (1 $\\mu$s). In this notebook, its data is loaded, processed and plotted.\n",
    "\n",
    "Let's load some basic modules: numpy (for basic operations with arrays) and pylab (for plotting)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import basic modules\n",
    "import numpy as np\n",
    "import pylab as pl\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic data access\n",
    "\n",
    "To access the GOLEM data, we write the function `get_data`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from urllib.error import HTTPError # recognise the error stemming from missing data\n",
    "import pandas as pd # for reading csv files\n",
    "\n",
    "#Define an exception which will be raised if the data is missing and stop the notebook execution\n",
    "class StopExecution(Exception):\n",
    "    def _render_traceback_(self):\n",
    "        pass\n",
    "\n",
    "def get_data(identifier, shot):\n",
    "    URL = \"http://golem.fjfi.cvut.cz/shots/{shot}/Diagnostics/PetiProbe/{identifier}.csv\"\n",
    "    url = URL.format(shot=shot, identifier=identifier)\n",
    "    try:\n",
    "        df = pd.read_csv(url, names=['time', identifier], index_col='time')\n",
    "    except HTTPError:\n",
    "        print('File not found at %s. Aborting notebook execution.' % url)\n",
    "        raise StopExecution\n",
    "    t = np.array(df.index)\n",
    "    data = np.transpose(np.array(df))[0]\n",
    "    return t, data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To test `get_data`, we load and plot the Langmuir probe data from the current discharge. The signal is called `U_LP`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "shot = 0\n",
    "t, data = get_data('U_fl_LP', shot)\n",
    "pl.plot(t*1000, data)\n",
    "pl.xlabel('$t$ [ms]')\n",
    "pl.ylabel('$V_{fl}$ [V]')\n",
    "pl.grid(True)\n",
    "pl.xlim(0,20)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that we multiply the time axis by a factor of 1000 in the argument of the `plot` function and then plot it in miliseconds, which yields nicer numbers on the X axis.\n",
    "\n",
    "## Discharge duration\n",
    "\n",
    "In the following, we define the function `shot_time(shot)` which loads the times of the discharge beginning and end."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from urllib.request import urlopen\n",
    "\n",
    "def get_time(phase, shot):\n",
    "    url = 'http://golem.fjfi.cvut.cz/shots/%i/Diagnostics/BasicDiagnostics/t_plasma_%s' % (shot, phase)\n",
    "    f = urlopen(url)\n",
    "    data = np.loadtxt(f)\n",
    "    f.close()\n",
    "    return float(data)/1000\n",
    "\n",
    "def shot_time(shot):\n",
    "    '''Return edges of time of shot in ms.'''\n",
    "    return get_time('start', shot), get_time('end', shot)\n",
    "\n",
    "t1, t2 = shot_time(shot)\n",
    "print('The discharge lasted from %.1f ms to %.1f ms.' % (t1*1000, t2*1000))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Offset removal\n",
    "\n",
    "An offset is a non-physical contribution to the signal, created by parasitic voltages, cross-talk between diagnostics, electronic noise and many other influences. In the simplest case, an offset is constant during the tokamak discharge. We can then calculate the offset by averaging our collected data during a time where they are supposed to be zero (typically before the discharge) and then subtract this value from the entire signal.\n",
    "\n",
    "Typically probe voltages do show offsets, as demonstrated in the following."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pl.plot(t[:1000]*1000, data[:1000])\n",
    "pl.xlabel('$t$ [ms]')\n",
    "pl.ylabel('$V_{fl}$ [V]')\n",
    "pl.grid(True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At the time $t=5$ ms ($t=0$ ms is globally the time when the data acquisition system starts recording diagnostic signals) the discharge is procedure is initiated. Since the sampling frequency is 1 MHz (distance 1 $\\mu$s between individual voltage samples), we average the voltage in the first 4000 samples (4 ms) to calculate the offset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "offset = data[:1000].mean()\n",
    "data = data - offset\n",
    "pl.plot(t*1000, data)\n",
    "pl.xlabel('$t$ [ms]')\n",
    "pl.ylabel('$V_{fl}$ [V]')\n",
    "pl.grid(True)\n",
    "pl.xlim(t1*1000-2,t2*1000+2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Electron temperature calculation\n",
    "\n",
    "To measure the electron temperature $T_e$, both the ball-pen probe and the Langmuir probe must be so-called *floating*. This means that they are electrically insulated from their surroundings and we measure the voltage which the plasma particles will charge them to. This *floating voltage* is different for both probes due to their different design. The ball-pen probe floating voltage is\n",
    "\n",
    "$V_{BPP} = \\Phi - 0.6T_e$\n",
    "\n",
    "where $\\Phi$ is the plasma potential, and the Langmuir probe floating voltage is\n",
    "\n",
    "$V_{fl} = \\Phi - 2.8T_e$.\n",
    "\n",
    "It then follows that the electron temperature can be calculated by subtracting the two floating voltages:\n",
    "\n",
    "$T_e = \\dfrac{V_{BPP}-V_{LP}}{2.2}.$\n",
    "\n",
    "In the following, both the probe signals are loaded and visualised to show that the ball-pen probe floating voltage is, indeed, higher than the Langmuir probe floating voltage."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the signals\n",
    "t, V_BPP = get_data('U_fl_BPP', shot)\n",
    "t, V_fl = get_data('U_fl_LP', shot)\n",
    "\n",
    "# Remove offsets\n",
    "V_BPP -= V_BPP[:1000].mean()\n",
    "V_fl -= V_fl[:1000].mean()\n",
    "\n",
    "# Plot the signals\n",
    "pl.plot(t*1000, V_fl, label='LP floating voltage')\n",
    "pl.xlabel('$t$ [ms]')\n",
    "pl.ylabel('$V_{fl}$ [V]')\n",
    "pl.plot(t*1000, V_BPP, label='BPP floating voltage')\n",
    "pl.xlabel('$t$ [ms]')\n",
    "pl.ylabel('$V_{BPP}$ [V]')\n",
    "pl.legend()\n",
    "pl.grid(True)\n",
    "pl.xlim(t1*1000-2,t2*1000+2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We then calculate the electron temperature by subtracting the two signals."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Te = (V_BPP-V_fl)/2.5\n",
    "pl.plot(t*1000, Te, label='Electron temperature')\n",
    "pl.xlabel('$t$ [ms]')\n",
    "pl.ylabel('$T_e$ [eV]')\n",
    "pl.legend()\n",
    "pl.grid(True)\n",
    "pl.xlim(t1*1000-2,t2*1000+2)\n",
    "pl.savefig('icon-fig')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
