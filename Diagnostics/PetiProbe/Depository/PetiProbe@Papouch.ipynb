{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Measurements with the combined probe head\n",
    "\n",
    "The combined probe head (ball-pen probe + Langmuir probe) offers measurements of the plasma potential $\\Phi$, floating potential $V_{fl}$ and electron temperature $T_e$ with high temporal resolution (1 $\\mu$s). In this notebook, its data is loaded, processed and plotted.\n",
    "\n",
    "Before that, let's load some basic modules: numpy (for basic operations with arrays), pandas (for storing signals) and pylab (for plotting)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import basic modules\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import pylab as plt\n",
    "from matplotlib import rc,rcParams\n",
    "rc('font', weight='bold')\n",
    "plt.rcParams['axes.labelweight'] = 'bold'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic data access\n",
    "\n",
    "To access the GOLEM data, we write the function `get_data`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_data(name, shot):\n",
    "    '''Return data of given diagnostic name in given shot.'''\n",
    "    data = pd.read_csv(f'http://golem.fjfi.cvut.cz/shots/{shot}/DASs/PetiProbe/{name}.csv', names = ['t','data'])\n",
    "    data.t = data.t*1000\n",
    "    data = data.set_index('t')\n",
    "    return data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To test `get_data`, we load the data of tunnel probe oriented toward limiter from the current discharge. The signal is called `1-Lim`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "shot = 0\n",
    "data = get_data('1-Lim', shot)\n",
    "data.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The data has two components: a time axis given in seconds and the voltage collected at that time. Let's plot the time evolution $V_{fl}(t)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = data.plot()\n",
    "ax.set_xlabel('t [ms]')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The interesting part of this plot is between roughly 0 ms and 15 ms. Using these time limits, we can replot the $V_{fl}(t)$ graph. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t1,t2 = [0, 15]\n",
    "ax = data.loc[t1:t2].plot()\n",
    "ax.set_xlabel('t [ms]')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Offset removal\n",
    "\n",
    "An offset is a non-physical contribution to the signal, created by parasitic voltages, cross-talk between diagnostics, electronic noise and many other influences. In the simplest case, an offset is constant during the tokamak discharge. We can then calculate the offset by averaging our collected data during a time where they are supposed to be zero (typically before the discharge) and then subtract this value from the entire signal.\n",
    "\n",
    "Typically probe voltages do show offsets, as demonstrated in the following."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = data[:0.5].plot()\n",
    "ax.set_xlabel('t [ms]')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At the time $t=5$ ms ($t=0$ ms is globally the time when the data acquisition system starts recording diagnostic signals) the discharge is procedure is initiated. Since the sampling frequency is 1 MHz (distance 1 $\\mu$s between individual voltage samples), we average the voltage in the first 500 samples (0.5 ms) to calculate the offset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "offset = data[:0.5].mean()\n",
    "data = data - offset\n",
    "ax = data[:0.5].plot()\n",
    "ax.set_xlabel('t [ms]')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Electron temperature calculation\n",
    "\n",
    "To measure the electron temperature $T_e$, both the ball-pen probe and the Langmuir probe must be so-called *floating*. This means that they are electrically insulated from their surroundings and we measure the voltage which the plasma particles will charge them to. This *floating voltage* is different for both probes due to their different design. The ball-pen probe floating voltage is\n",
    "\n",
    "$V_{BPP} = \\Phi - 0.3T_e$\n",
    "\n",
    "where $\\Phi$ is the plasma potential, and the Langmuir probe floating voltage is\n",
    "\n",
    "$V_{fl} = \\Phi - 2.8T_e$.\n",
    "\n",
    "It then follows that the electron temperature can be calculated by subtracting the two floating voltages:\n",
    "\n",
    "$T_e = \\dfrac{V_{BPP}-V_{LP}}{2.5}.$\n",
    "\n",
    "In the following, both the probe signals are loaded and visualised to show that the ball-pen probe floating voltage is, indeed, higher than the Langmuir probe floating voltage."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the signals\n",
    "V_BPP = get_data('3-BPP', shot)*100\n",
    "V_fl = get_data('4-LP', shot)*100\n",
    "\n",
    "# Remove offsets\n",
    "V_BPP -= V_BPP[:0.5].mean()\n",
    "V_fl -= V_fl[:0.5].mean()\n",
    "\n",
    "\n",
    "# Plot the signals\n",
    "ax1=V_BPP.loc[t1:t2].plot()\n",
    "ax1.legend(['BPP'])\n",
    "ax1.set_xlabel('t [ms]')\n",
    "ax1.set_ylabel('U [V]')\n",
    "ax2=V_fl.loc[t1:t2].plot()\n",
    "ax2.legend(['LP'])\n",
    "ax1.set_xlabel('t [ms]')\n",
    "ax2.set_ylabel('U [V]')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We then calculate the electron temperature by subtracting the two signals."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Te = (V_BPP-V_fl)/2.5\n",
    "ax = Te.loc[t1:t2].plot()\n",
    "ax.set_xlabel('t [ms]')\n",
    "ax.set_ylabel(r'$T_\\mathrm{e}$ [eV]')\n",
    "ax.legend([r'$T_\\mathrm{e}$'])\n",
    "plt.savefig('icon-fig.jpg')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy import signal"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Mach number calculation:\n",
    "\n",
    "$M = \\frac{1}{4} \\ln(\\frac{TP_\\mathrm{olim}}{TP_\\mathrm{lim}})$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Lim = get_data('1-Lim', shot).rolling(50).mean()\n",
    "#Lim.data = signal.savgol_filter(Lim.data,101,1)\n",
    "oLim = get_data('2-OLim', shot).rolling(50).mean()\n",
    "#oLim.data = signal.savgol_filter(oLim.data,101,1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Mpar = 0.25*np.log(oLim/Lim)\n",
    "Mpar.loc[t1:t2].plot()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
