{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Measurements with the combined probe head\n",
    "\n",
    "The combined probe head (ball-pen probe + Langmuir probe) offers measurements of the plasma potential $\\Phi$, floating potential $V_{fl}$ and electron temperature $T_e$ with high temporal resolution (1 $\\mu$s). In this notebook, its data is loaded, processed and plotted.\n",
    "\n",
    "Before that, let's load some basic modules: numpy (for basic operations with arrays), xarray (for storing signals including labels, units etc.) and pylab (for plotting)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import basic modules\n",
    "import numpy as np\n",
    "import xarray as xr\n",
    "import pylab as pl\n",
    "\n",
    "# Instruct xarray to preserve DataArray attributes through arithmetic operations (namely offset removal)\n",
    "xr.set_options(keep_attrs=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic data access\n",
    "\n",
    "To access the GOLEM data, we write the function `get_data`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from urllib.request import urlopen\n",
    "\n",
    "def get_data(name, shot):\n",
    "    '''Return data of given diagnostic name in given shot.'''\n",
    "    url = 'http://golem.fjfi.cvut.cz/utils/data/%i/%s' % (shot, name)\n",
    "    f = urlopen(url)\n",
    "    data = np.loadtxt(f)\n",
    "    f.close()\n",
    "    return np.transpose(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To test `get_data`, we load the Langmuir probe data from discharge #25993. The signal is called `lp`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "shot = 25993\n",
    "data = get_data('lp', shot)\n",
    "print(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The data has two components: a time axis given in seconds and the voltage collected at that time. Let's plot the time evolution $V_{fl}(t)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t = data[0]*1000\n",
    "Vfl = data[1]\n",
    "pl.plot(t, Vfl)\n",
    "pl.xlabel('$t$ [ms]')\n",
    "pl.ylabel('$V_{fl}$ [V]')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that we multiply the time axis by a factor of 1000 in the argument of the `plot` function and then plot it in miliseconds, which yields nicer numbers on the X axis.\n",
    "\n",
    "The interesting part of this plot is between the plasma start at roughly 10 ms and plasma end at roughly 25 ms. We can load these times automatically for any discharge using the following function, `shot_time`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def shot_time(shot):\n",
    "    '''Return edges of time of shot in ms.'''\n",
    "    t1 = float(get_data('plasma_start', shot))*1000\n",
    "    t2 = float(get_data('plasma_end', shot))*1000\n",
    "    return t1, t2\n",
    "\n",
    "print(shot_time(25984))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using these time limits, we can replot the $V_{fl}(t)$ graph."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t1, t2 = shot_time(shot)\n",
    "discharge = (t1 <= t) & (t < t2)\n",
    "pl.plot(t[discharge], Vfl[discharge])\n",
    "pl.xlabel('$t$ [ms]')\n",
    "pl.ylabel('$V_{fl}$ [V]')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conversion of GOLEM data into xarray DataArrays\n",
    "\n",
    "The package `xarray` offers a faster and more flexible alternative to the signal processing shown in the previous section. We will work with the same signal.\n",
    "\n",
    "First, let's define an alternative to `get_data` which returns the data in the form of a `DataArray`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_DA(name, shot):\n",
    "    t, data = get_data(name, shot)\n",
    "    \n",
    "    signal_dict = {'lp': ['Langmuir probe', 'V', 100, '$V_{fl}$'],\n",
    "                   'bpp': ['Ball-pen probe', 'V', 100, '$V_{BPP}$'],\n",
    "                   'loop_voltage': ['Loop voltage', 'V', 1, '$U_{loop}$'],\n",
    "                   'plasma_current': ['Plasma current', 'A', 1, '$I_p$'],\n",
    "                   'electron_density': ['Line-averaged electron density', 'm$^{-3}$', 1, '$\\overline{n}_e$']\n",
    "                  }\n",
    "    \n",
    "    DA = xr.DataArray(data*signal_dict[name][2],\n",
    "                      dims=['time'],\n",
    "                      coords={'time': t*1000, 'shot': shot}\n",
    "                     )\n",
    "    \n",
    "    DA.name = signal_dict[name][0]\n",
    "    DA.attrs = {'units': signal_dict[name][1],\n",
    "                'signal_name': name,\n",
    "                'standard_name': signal_dict[name][3],\n",
    "               }\n",
    "    DA.time.attrs = {'units': 'ms',\n",
    "                     'standard_name': '$t$',\n",
    "                    }\n",
    "    \n",
    "    return DA\n",
    "\n",
    "data = get_DA('lp', shot)\n",
    "data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using this `DataArray` instance, plotting is much easier."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To cut the data into the desired time frame:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.loc[t1:t2].plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Offset removal\n",
    "\n",
    "An offset is a non-physical contribution to the signal, created by parasitic voltages, cross-talk between diagnostics, electronic noise and many other influences. In the simplest case, an offset is constant during the tokamak discharge. We can then calculate the offset by averaging our collected data during a time where they are supposed to be zero (typically before the discharge) and then subtract this value from the entire signal.\n",
    "\n",
    "Typically probe voltages do show offsets, as demonstrated in the following."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data[:4000].plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At the time $t=5$ ms ($t=0$ ms is globally the time when the data acquisition system starts recording diagnostic signals) the discharge is procedure is initiated. Since the sampling frequency is 1 MHz (distance 1 $\\mu$s between individual voltage samples), we average the voltage in the first 4000 samples (4 ms) to calculate the offset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "offset = data[:4000].mean()\n",
    "data = data - offset\n",
    "data[:4000].plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Electron temperature calculation\n",
    "\n",
    "To measure the electron temperature $T_e$, both the ball-pen probe and the Langmuir probe must be so-called *floating*. This means that they are electrically insulated from their surroundings and we measure the voltage which the plasma particles will charge them to. This *floating voltage* is different for both probes due to their different design. The ball-pen probe floating voltage is\n",
    "\n",
    "$V_{BPP} = \\Phi - 0.6T_e$\n",
    "\n",
    "where $\\Phi$ is the plasma potential, and the Langmuir probe floating voltage is\n",
    "\n",
    "$V_{fl} = \\Phi - 2.8T_e$.\n",
    "\n",
    "It then follows that the electron temperature can be calculated by subtracting the two floating voltages:\n",
    "\n",
    "$T_e = \\dfrac{V_{BPP}-V_{LP}}{2.2}.$\n",
    "\n",
    "In the following, both the probe signals are loaded and visualised to show that the ball-pen probe floating voltage is, indeed, higher than the Langmuir probe floating voltage."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the signals\n",
    "V_BPP = get_DA('bpp', shot)\n",
    "V_fl = get_DA('lp', shot)\n",
    "\n",
    "# Remove offsets\n",
    "V_BPP -= V_BPP[:4000].mean()\n",
    "V_fl -= V_fl[:4000].mean()\n",
    "\n",
    "# Plot the signals\n",
    "V_BPP.loc[t1:t2].plot(label=V_BPP.attrs['standard_name'])\n",
    "V_fl.loc[t1:t2].plot(label=V_fl.attrs['standard_name'])\n",
    "pl.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We then calculate the electron temperature by subtracting the two signals."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Te = (V_BPP-V_fl)/2.2\n",
    "Te.attrs = {'standard_name': '$T_e$',\n",
    "            'units': 'eV',\n",
    "           }\n",
    "Te.name = 'Electron temperature'\n",
    "Te.loc[t1:t2].plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Profile calculation\n",
    "\n",
    "The combined probe head cannot move during a GOLEM discharge, and thus it is stuck measuring at one location for the entire discharge. This is why probe measurements are often done on a *shot-to-shot basis*, where the probe is moved inbetween otherwise identical discharges and thus gradually collects data from the entire profile.\n",
    "\n",
    "An important part of measuring profiles on a *shot-to-shot basis* is to make sure the discharges are really identical. To this end we plot the basic plasma parameters, loop voltage $U_{loop}$, plasma current $I_p$ and the line-averaged plasma density $\\overline{n}_e$. The toroidal magnetic field $B_t$ is externally imposed, and thus will be identical automatically.\n",
    "\n",
    "For demonstration purposes, we will use the discharges made by Petr Mácha on 18 January 2018 as part of his bachelor's thesis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Discharge numbers\n",
    "SHOTLIST = np.array([25984, 25987, 25989, 25990, 25992, 25993, 25994, 25995, 25997, 25998, 25999])\n",
    "\n",
    "#Distance of the probes from the tokamak chamber centre in mm\n",
    "R = np.array([105, 100, 95, 90, 85, 80, 75, 70, 65, 60, 55])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To investigate a few discharges at once, we first defince a function `get_DAs` which can load data of the same signal name from multiple discharges. Notice the function `xr.concat`, which concatenates the individual 1D arrays (with used coordinate `'time'` and unused coordinate `shot`) into a 2D array (with both cooordinates active)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_DAs(name, shotlist):\n",
    "    DAs = [get_DA(name, shot) for shot in shotlist]\n",
    "    return xr.concat(DAs, 'shot')\n",
    "\n",
    "get_DAs('lp', SHOTLIST[:3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using this `get_DAs`, we easily load and plot all the desired signals."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def check_shot_identity(shotlist):\n",
    "    # Load the signals\n",
    "    U_loop = get_DAs('loop_voltage', shotlist)\n",
    "    Ip = get_DAs('plasma_current', shotlist)\n",
    "    ne = get_DAs('electron_density', shotlist)\n",
    "    \n",
    "    # Plot the signals\n",
    "    fig, axes = pl.subplots(3, figsize=(6,10))\n",
    "    U_loop.plot.line(x='time', ax=axes[0])\n",
    "    Ip.plot.line(x='time', ax=axes[1])\n",
    "    ne.plot.line(x='time', ax=axes[2])\n",
    "    return\n",
    "\n",
    "check_shot_identity(SHOTLIST[:3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Evidently, the discharges are similar but not identical. This level of fluctuations is usually acceptable in profile measurements. However, since the discharges all begin at different times, it isn't convenient to take profiles \"3 ms after discharge beginning\", which is a slightly different time for each discharge. Rather we define a set of fixed times, spaced by 1 ms, when the probe measurements are averaged and the profile is taken."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "TIMES = [9, 25]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we write the function `convert_to_profiles`, which takes a set of data measurements (created by `get_DAs`) and a series of time points and constructs a profile measured at these time points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = get_DAs('lp', SHOTLIST[:3])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def convert_to_profiles(data, dt=1):\n",
    "    '''dt ... profile point spacing in ms'''\n",
    "    means = data.rolling(time=dt*1000).mean()\n",
    "    return means\n",
    "convert_to_profiles(data)    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
