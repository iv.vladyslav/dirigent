#!/bin/bash

whoami="Diagnostics/PetiProbe/20_SP_DaCi_JiAd_IonTemp"

# Tuning purposes:
#cp /golem/svoboda/Dirigent/DASs/Papouch-St/Papouch-St.sh /dev/shm/golem/ActualShot/DASs/Papouch-St/;cd /dev/shm/golem/ActualShot/Diagnostics/PetiProbe/;cp /golem/svoboda/Dirigent/Diagnostics/PetiProbe/20_SP_DaCi_JiAd_IonTemp.sh .;source 20_SP_DaCi_JiAd_IonTemp.sh ;PostDischargeAnalysis;ll

DAS="Oscilloscopes/TektrMSO58-a/20_SP_JirkaA_DarioC_Petiprobe_IonTemp"

Devices="RelayBoards/Energenie_LANpower-a/PowerSupply4DASs FunctionGenerators/RigolDG1032Z-a/FG4ElstatProbes $DAS"


LastChannelToAcq=4
diags=('U_loop' 'I_BPP' 'U_BPP' 'U_LP')

function PostDischargeAnalysis() 
{


    DiagnosticsRequestToDASToGetData $DAS $LastChannelToAcq

    for i in `seq 1 $LastChannelToAcq` ; do
        cp $SHM0/DASs/$DASs/ch$i.csv ${diags[$i-1]}.csv
    done    

    GenerateWWWs
}


function GenerateWWWs
{
#echo '<HTML><META HTTP-EQUIV="Refresh" CONTENT="0;URL=
#http://golem.fjfi.cvut.cz/shots/'`cat $BASEDIR/shot_no`'XXXYYY/analysis.html"><HEAD><TITLE></TITLE><BODY></BODY></HTML>' > analysis.html


    Analysis


    echo '<HTML><HEAD><TITLE></TITLE><BODY>
    <H1>Raw data</H1>
    <img src="http://golem.fjfi.cvut.cz/shots/'`cat $BASEDIR/shot_no`'/DASs/PetiProbe/graph1.png"></img><br></br>
    <a href="http://golem.fjfi.cvut.cz/shots/'`cat $BASEDIR/shot_no`'/DASs/PetiProbe/">Data directory</a>
    
    </BODY></HTML>' > rawdata.html
    
    echo '
    <H2>On Stage Diagnostics: '`cat $SHMS/session_mission`' </H2>
    <img src="Diagnostics/'$ThisDev'/icon-fig.png"></img><br></br>
    ' > onstage.html
    
    
    echo "<tr>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$ThisDev/><img src=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$ThisDev/name.png  width='$namesize'/></a></td>
    <td style='height:'$iconsize'' valign=bottom ><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$ThisDev/setup.html><img src=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$ThisDev/setup.png  /></a></td>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/DASs/$DASs/analysis.html><img src=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/DASs/$DASs/das.jpg  width='$iconsize'/></a></td>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/DASs/$DASs/><img src=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/DASs/$DASs/icon.png  width='$iconsize'/></a></td>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$ThisDev/analysis.html><img src=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$ThisDev/analysis.jpg  width='$iconsize'/></a></td>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$ThisDev/><img src=http://golem.fjfi.cvut.cz/_static/direct.png  width='100px'/></a></td>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$ThisDev/DataProcessing/DataProcessing.html><img src=http://golem.fjfi.cvut.cz/_static/DataProc.png  width='100px'/></a></td></tr></tbody></table>" > diagrow.html;
    
    
    ln -s ../../DASs/PetiProbe/ DAS_raw_data_dir
    
    

}

function Analysis
{
    echo -n "set terminal png;unset xtics;set size 1,1;set origin 0,0;set multiplot layout $LastChannelToAcq,1 columnsfirst scale 1.1,1;set datafile separator ',';" >/tmp/foo; for i in `seq 1 $LastChannelToAcq`; do echo -n plot \'${diags[$i-1]}'.csv'\' u 1:2';';done >>/tmp/foo;echo " unset multiplot" >>/tmp/foo; cat /tmp/foo|gnuplot > icon-fig.png

convert -resize $icon_size icon-fig.png analysis.jpg
}

