#sed "s/ShotNo=0;/ShotNo=`cat /golem/database/operation/shots/0/shot_no`/g" Script.m > ScriptWithShotNo.m
sed "s/ShotNo=0;/ShotNo=`cat /golem/database/operation/shots/39789/shot_no`/g" Script.m > ScriptWithShotNo.m
matlab -nosplash -nodesktop -r ScriptWithShotNo
convert icon-fig.jpg icon-fig.png
convert -resize 200 icon-fig.jpg graph.png
rm index.html
webpage=analysis.html
sas=`dirname $PWD|xargs basename`
echo "<h1>$sas: Data processing for GOLEM </h1>" > $webpage

#cat ScriptWithShotNo.m >>$webpage
echo "</pre><h2>Result:</h2><img src=icon-fig.jpg width='30%'></img>" >>$webpage

