BASEDIR="../.."
source $BASEDIR/Commons.sh


diag_id="TrainingCourses"
setup_id="RigolB_RigolD"
whoami="Diagnostics/$diag_id/$setup_id"

#b,d
#DAS="Oscilloscopes/RigolMSO5204-b/RemoteTrainingOsc-b Oscilloscopes/RigolMSO5204-b/RemoteTrainingOsc-d"

#b,e
DAS="Oscilloscopes/RigolMSO5204-b/RemoteTrainingOsc-b Oscilloscopes/RigolMSO5204-e/RemoteTrainingOsc-e"

Devices="$DAS"


LastChannelToAcq=4
diags=('ch1' 'ch2' 'ch3' 'ch4' )

function PostDischargeAnalysis() 
{

    GeneralDAScommunication $DAS RawDataAcquiring $LastChannelToAcq
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir

    for i in `seq 1 $LastChannelToAcq` ; do
        cp DAS_raw_data_dir/ch$i.csv ${diags[$i-1]}.csv
    done    

    Analysis
    GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` # @Commons.sh
    
}


# -b,-d,-e
# for address in 11:22 19:45 14:7b;do sudo arp-scan --interface=global0 147.32.4.0/24|grep $address;sudo arp-scan --interface=global0 147.32.5.0/24|grep $address; done
