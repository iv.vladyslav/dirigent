# -b,-d,-e
# for address in 11:22 19:45 14:7b;do sudo arp-scan --interface=global0 147.32.4.0/24|grep $address;sudo arp-scan --interface=global0 147.32.5.0/24|grep $address; done

function CommonPostDischargeAnalysis()
{
    GeneralDAScommunication $DAS RawDataAcquiring
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir-$line
    NoAnalysis
    convert -resize $icon_size icon-fig-$line.png graph-$line.png
    CommonGenerateDiagWWWs $diag_id $setup_id `dirname $DAS` "Nan" "Nan" $line
}

function NoAnalysis()
{
    cp $SW_dir/Management/imgs/Commons/WithoutAnalysis.png icon-fig-$line.png
}

function Analysis()
{
    sed "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" /golem/Dirigent/Diagnostics/BasicDiagnostics4TrainCourses/commons/StandardDAS.ipynb > StandardDAS.ipynb
    sed -i "s/Rigol_line\ =\ 'xy'/Rigol_line\ =\ \'$ThisDev\'/g" StandardDAS.ipynb
    jupyter-nbconvert --execute StandardDAS.ipynb --to html --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)

}


function CommonOpenSession()
{
    SCALE=10
    OFFSET=0
    echo "
    :CHANnel1:DISPlay ON;CHANnel1:PROBe 1;CHANnel1:SCALe $SCALE;CHANnel1:OFFSet $OFFSET;
    :CHANnel2:DISPlay ON;CHANnel2:PROBe 1;CHANnel2:SCALe $SCALE;CHANnel2:OFFSet $OFFSET;
    CHANnel3:DISPlay ON;CHANnel3:PROBe 1;CHANnel3:SCALe $SCALE;CHANnel3:OFFSet $OFFSET;
    CHANnel4:DISPlay ON;CHANnel4:PROBe 1;CHANnel4:SCALe $SCALE;CHANnel4:OFFSet $OFFSET;
    :TIMebase:DELay:ENABle OFF;TIMebase:MAIN:SCALe 0.001;TIMebase:MAIN:OFFSet 0;
    :TRIGger:EDGE:SOURce CHANnel1;TRIGger:EDGE:SLOPe POS;TRIGger:EDGE:LEVel 0;
    :STOP;:CLEAR;
    :SYSTem:KEY:PRESs MOFF"|$COMMAND
    echo OK
}

#Vyhazuje chybovou hlasku, coz je "OK"
#golem@Dirigent>source Setups/Praktika.setup;for i in `for i in $OnStage_wave;do echo $i;done|grep Train`;do echo `basename $i`;source $i.sh;cd Devices/`dirname $DAS`; pwd;source `basename $DAS`.sh;CommonSampleSetting; cd -;done


function CommonSampleSetting()
{
    echo "
    :CHANnel1:DISPlay ON;CHANnel1:PROBe 1;CHANnel1:SCALe 3;CHANnel1:OFFSet -10;
    :CHANnel2:DISPlay ON;CHANnel2:PROBe 1;CHANnel2:SCALe 0.02;CHANnel2:OFFSet -0.06;
    :CHANnel3:DISPlay ON;CHANnel3:PROBe 1;CHANnel3:SCALe 0.1;CHANnel3:OFFSet 0;
    :CHANnel4:DISPlay ON;CHANnel4:PROBe 1;CHANnel4:SCALe 0.1;CHANnel4:OFFSet 0.0;
    :TIMebase:DELay:ENABle OFF;TIMebase:MAIN:SCALe 0.002;TIMebase:MAIN:OFFSet 0.008;
    :TRIGger:EDGE:SOURce CHANnel1;TRIGger:EDGE:SLOPe POS;TRIGger:EDGE:LEVel 6;
    :STOP;:CLEAR;
    :SYSTem:KEY:PRESs MOFF"|$COMMAND
    echo OK
}


function CommonGetData()
{
#SETUP
    mkdir -p ScopeSetup;quer='XINC XOR XREF START STOP'; echo `for i in $quer; do echo :WAV:$i?";";done`'*IDN?'|nc -w 1 $ThisDev 5555|sed 's/;/\n/g'|sed \$d > ScopeSetup/answers;for i in $quer; do echo _WAV_$i;done > ScopeSetup/querries;paste ScopeSetup/querries ScopeSetup/answers > ScopeSetup/ScopeSetup; 
    #cat ScopeSetup/ScopeSetup;
    awk '{print "echo " $2 " >ScopeSetup/"$1}' ScopeSetup/ScopeSetup |bash;

#TIME
    _WAV_XINC=`cat ScopeSetup/_WAV_XINC`;_WAV_START=`cat ScopeSetup/_WAV_START`;_WAV_STOP=`cat ScopeSetup/_WAV_STOP`; perl -e '$Time=0;for ( $i='$_WAV_START' ; $i <= '$_WAV_STOP'; $i++ ){printf "%.5e\n", $Time;$Time=$Time+'$_WAV_XINC'}' > Time
    
#DATA
    for CHANNEL in `seq 1 4`; do 
	(echo "$ThisDev oscilloscope:  Downloading data for channel $CHANNEL";echo ":WAV:SOURCE CHAN$CHANNEL;:WAV:FORM ASCii;:WAV:MODE NORM;:WAV:DATA?" |$scope_address|cut -c 12-|sed 's/,/\n/g'|tee data$CHANNEL|gnuplot -e 'set terminal jpeg;plot "<cat" w l' > ${diags[$CHANNEL]}.jpg;paste -d, Time data$CHANNEL > ${diags[$CHANNEL]}.csv) &
    sleep 0.2s
    done
    wait
    rm Time data
#    paste data* > data_all
#    Analysis # Pro ucitele
}


function CommonGenerateDiagWWWs  
{
local diag_id=$1
local setup_id=$2
local DASId=$3
local GWdiagpath="http://golem.fjfi.cvut.cz/wiki/$4"
local googlephotospath="https://photos.app.goo.gl/$5"
local line=$6

#local diagpath="http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$diag_id"
local diagpath="Diagnostics/$diag_id"
local devicepath="Devices/$DASId"
      
    echo "<tr>
    <td valign=bottom><a href=$diagpath/><img src=$diagpath/figs/name-$line.png  width='$namesize'/></a></td>
    
    <td valign=bottom><a href=$diagpath/expsetup.svg><img src=$diagpath/setup.png /></a></td>
    <td><a href=$GWdiagpath title="GW path"><img src=$imgpath/logos/golem.png  width='$linkiconsize'/></a><br></br>
    <a href=$googlephotospath title="Photogallery@Google">$googlephotosicon</a><br></br></td>
    
    <td valign=bottom><a href=$devicepath/das.html><img src=$devicepath/das.jpg  width='$iconsize'/></a></td>
    <td><a href=$devicepath/das.html title="Manual$devicepath">$manualicon</a><br></br>
    </td>
    
    <td valign=bottom><a href=$devicepath/ScreenShotAll-$line.png><img src=$devicepath/rawdata-$line.jpg  width='$iconsize'/></a></td>
    <td>
    <a href=$gitlabpath/$devicepath/ title="Gitlab4$devicepath">$gitlabicon</a><br></br>
    <a href=$devicepath/ title="Data directory">$diricon</a><br></br>
    </td>
    
    
    <td valign=bottom><a href=$diagpath/analysis-$line.html><img src=$diagpath/graph-$line.png  width='$iconsize'/></a></td>
    <td>
    <a href=$diagpath/Parameters/ title="Parameters">$parametersicon</a><br></br>
    <a href=$gitlabpath/Diagnostics/$diag_id/commons title="JupyterNotebook$diagpath">$pythonicon</a>
    $rightarrowicon
    <a href=$diagpath/analysis-$line.html title="Analysis$diagpath">$resultsicon</a><br></br>
    <a href=$gitlabpath/$diagpath/ title="Gitlab4$diagpath">$gitlabicon</a>
    <a href=$diagpath/ title="Directory">$diricon</a>
    <a href=$dbpath/Diagnostics/$diag_id/ title="Database">$sqlicon</a>
    </td></tr>" >diagrow_$setup_id.html;
    
    
    echo "<center><h1>The GOLEM tokamak Basic Diagnostics setup</h1>
    <h2>Photo</h2>
    <img src="/_static/figs/DAS-d_s.jpg" width="25%"></img><br/>
    <h2>Relevant excerpt from the manual</h2>" > setup.html;
    for j in `seq 0 2`; do
        echo "<img src=http://golem.fjfi.cvut.cz/wiki/Education/GMinstructions/extracts/Extracts/Diagnostics/output-$j.jpg width='50%'></img>" >> setup.html;
    done
    
    
}




function CommonSandbox()
{
    mkdir -p ScopeSetup;quer='XINC XOR XREF START STOP'; echo `for i in $quer; do echo :WAV:$i?";";done`'*IDN?'|nc -w 1 RemoteTrainingOsc-b.golem 5555|sed 's/;/\n/g'|sed \$d > ScopeSetup/answers;for i in $quer; do echo _WAV_$i;done > ScopeSetup/querries;paste ScopeSetup/querries ScopeSetup/answers > ScopeSetup/ScopeSetup; cat ScopeSetup/ScopeSetup;awk '{print "echo " $2 " >ScopeSetup/"$1}' ScopeSetup/ScopeSetup |bash;ll ScopeSetup/

}

function CommonSampleSetting_zmb()
{
    SCALE=10
    OFFSET=0
    #Uloop
    echo "CHANnel1:DISPlay ON;CHANnel1:PROBe 1;CHANnel1:SCALe 3;CHANnel1:OFFSet -10"|$COMMAND 
    #Bt
    echo "CHANnel2:DISPlay ON;CHANnel2:PROBe 1;CHANnel2:SCALe 0.02;CHANnel2:OFFSet -0.06"|$COMMAND           
    #I_{p+ch}
    echo "CHANnel3:DISPlay ON;CHANnel3:PROBe 1;CHANnel3:SCALe 0.1;CHANnel3:OFFSet 0"|$COMMAND 
    #Photod
    echo "CHANnel4:DISPlay ON;CHANnel4:PROBe 1;CHANnel4:SCALe 0.1;CHANnel4:OFFSet 0.0"|$COMMAND 
    #Timebase
    echo "TIMebase:DELay:ENABle OFF;TIMebase:MAIN:SCALe 0.002;TIMebase:MAIN:OFFSet 0.008"|$COMMAND
    echo ":TRIGger:EDGE:SOURce CHANnel1;TRIGger:EDGE:SLOPe POS;TRIGger:EDGE:LEVel 6"|$COMMAND
    echo ":STOP;:CLEAR"|$COMMAND
    echo ":SYSTem:KEY:PRESs MOFF"|$COMMAND
    echo OK
}


#source Commons.sh ;PostDischargeAnalysisTest file:///golem/shm_golem/ActualShot/Diagnostics/BasicDiagnostics4TrainCourses/StandardDAS-b.sh
