#!/bin/bash

SHM="/dev/shm/golem"
source $SHM/Commons.sh
rPi=192.168.2.92 ## rPI IP adress 
shot_no=`CurrentShotDataBaseQuerry shot_no`

diag_id="TimepixDetector"
setup_id="TimepixDetector"
whoami="Diagnostics/$diag_id/$setup_id"

DAS="Computers/RASP-TimepixDetector/TimepixDetector"
Devices="Computers/RASP-TimepixDetector/TimepixDetector"

function Arming()
{
	ssh pi@$rPi "rm data/*"
	ssh pi@$rPi "python3 test2.py $shot_no > /dev/null 2>/dev/null &"
	#ssh pi@$rPi "python3 test2.py $shot_no  &"
	#ssh pi@$rPi "python3 test2.py $shot_no"
}




function PostDischargeAnalysis()
{
      sleep 5
       scp pi@RASP-TimepixDetector:data/* .
       GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` $GooglePhotosPath # @Commons.sh

}

