source /dev/shm/golem/Commons.sh


Devices="AVs/PhotronMiniUX50-a/Radial AVs/PhotronMiniUX50-b/Vertical ITs/PhotronCamerasPC/FastCameras"

function DefineTable()
{
    echo OK;    
#    CreateTable discharge.position_stabilization
}


function WakeOnLan(){ 
    echo "*B1OS7H"|telnet 192.168.2.254 10001 1>&- 2>&-; # 
 } 

function SleepOnLan(){ 
    echo "*B1OS7L"|telnet 192.168.2.254 10001 1>&- 2>&-; # 
}

function OpenSession()
{
    echo OK;    
#    InitFunctGen 
}


function Arming()
{
    echo OK;    
}

function GetReadyTheDischarge ()
{
    echo OK;
    #GeneralTableUpdateAtDischargeBeginning diagnostics.$diag_id #@Commons.sh
}




function PostDischargeAnalysis
{
    #convert -resize $icon_size SpeedCamera.png analysis.jpg
    timeout=20
    while ! test -s /mnt/share/PhotronCamerasPC/CamRad.avi;
    do
        if [ "$timeout" == 0 ]; then
        LogItColor 1 "ERROR: Timeout while waiting for the file from $ThisDev"
        exit 1
    fi
    sleep 1
    echo $timeout s to wait for $ThisDev files
    ((timeout--))
    done

    while ! test -s /mnt/share/PhotronCamerasPC/CamVert.avi;
    do
        if [ "$timeout" == 0 ]; then
        LogItColor 1 "ERROR: Timeout while waiting for the file from $ThisDev"
        exit 1
    fi
    sleep 1
    echo $timeout s to wait for $ThisDev files
    ((timeout--))
    done


    sleep 10
    mkdir Camera_Radial
    mkdir Camera_Vertical
    #cp /mnt/share/`ls -1tr /mnt/share/|grep C002|tail -1`/*.* Camera_Radial/
    cp /mnt/share/PhotronCamerasPC/CamRad.avi Camera_Radial/Data.avi
    cp /mnt/share/PhotronCamerasPC/CamVert.avi Camera_Vertical/Data.avi
    
    sed -i "s/shot_no=0/shot_no\ =\ `cat $SHMS/shot_no`/g" Camera_Position.ipynb 
    jupyter-nbconvert --ExecutePreprocessor.timeout=60 --to html --execute Camera_Position.ipynb --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)
    convert -resize $icon_size icon-fig.png graph.png
    
    
    GenerateDiagWWWs $diag_id $setup_id ../Diagnostics/FastCameras # @Commons.sh
}



