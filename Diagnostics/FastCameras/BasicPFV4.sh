source /dev/shm/golem/Commons.sh

Devices="ITs/PhotronCamerasPC/FastCameras AVs/PhotronMiniUX50-a/Radial AVs/PhotronMiniUX50-b/Vertical"


function DefineTable()
{
    echo OK;    
#    CreateTable discharge.position_stabilization
}



function PostDischargeAnalysis
{
    #convert -resize $icon_size SpeedCamera.png analysis.jpg
    sleep 25
    mkdir Camera_Radial
    mkdir Camera_Vertical
    cp /mnt/share/`ls -1tr /mnt/share/|grep C002|tail -1`/*.* Camera_Radial/
    cp /mnt/share/`ls -1tr /mnt/share/|grep C001|tail -1`/*.* Camera_Vertical/
    
    sed -i "s/shot_no=0/shot_no\ =\ `cat $SHMS/shot_no`/g" Camera_Position.ipynb 
    jupyter-nbconvert --ExecutePreprocessor.timeout=60 --to html --execute Camera_Position.ipynb --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)
    convert -resize $icon_size icon-fig.png graph.png
    
    
    GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` Diagnostics/FastCameras # @Commons.sh
}


