#!/bin/bash

source ../../Commons.sh

diag_id="Calorimeter"
setup_id="1121_JC_basic"
whoami="Diagnostics/$diag_id/$setup_id"

DAS="Oscilloscopes/RigolMSO5204-d/Calorimeter"

Devices="$DAS"


function PostDischargeAnalysis()
{
    sleep 15s
   GeneralDAScommunication $DAS RawDataAcquiring
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir

    cp DAS_raw_data_dir/* .
    

    GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` # @Commons.sh
}


