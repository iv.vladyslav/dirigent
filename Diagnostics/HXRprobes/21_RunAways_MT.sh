BASEDIR="../.."
source $BASEDIR/Commons.sh



diag_id="HXRprobes"
setup_id="21_RunAways_MT"
whoami="Diagnostics/$diag_id/$setup_id"

DAS="Oscilloscopes/TektrMSO58-a/21_RunAways_MT"


Devices="PowerSupplies/NIMcrate/HXRdiagnostics $DAS"


HXR_probes="CeBr_a YAP_a NaITl_a ZnSAg_a LYSO"
#(x,y,z, theta, phi, HV_PS@NIMcrate, HVchannel@NIMcrate, Voltage)
S_CeBr_a=(380 130 72 180 0 A 0 -700)
S_YAP_a=(380 130 72 180 0 A 1 -800)
S_NaITl_a=(380 130 72 180 0 A 2 -700)
S_ZnSAg_a=(380 130 72 180 0 A 3 -700)


LastChannelToAcq=6

diags=('U_Loop-b' 'YAP-a' 'CeBr-a' 'CeBr-b' 'NaITl-a' 'LYSO' 'Trigger')


function WakeOnLan(){ 
sleep 10 # wait for NIM crate to be ready
ssh svoboda@bn 'cd /golem/Dirigent/Devices/PowerSupplies/NIMcrate/; for i in `seq 0 3`;do source HXRdiagnostics.sh;SetupHV@channel 600 $i;done'
} 

function SleepOnLan(){ 
    sleep 1
}

function GetReadyTheDischarge ()
{
  GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh
}

function PostDischargeAnalysis()
{
  
   sleep 4
    GeneralDAScommunication $DAS RawDataAcquiring
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir

    for i in `seq 1 $LastChannelToAcq` ; do
        cp DAS_raw_data_dir/ch$i.csv ${diags[$i-1]}.csv
    done    

    GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` # @Commons.sh
    Analysis
}




function NoAnalysis()
{
    cp $SW_dir/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
    convert -resize $icon_size icon-fig.png graph.png
}


function Analysis()
{

    local nb_id="21_RunAways_MT"

    #export SHOT_NO=`cat ../../shot_no` #linux only
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat $SHMS/shot_no`/g" $nb_id.ipynb
    
    jupyter-nbconvert --ExecutePreprocessor.timeout=300 --to html --execute $nb_id.ipynb --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)
    

    if [[ -f icon-fig.png ]]; then
        convert -resize $icon_size icon-fig.png graph.png
        convert -resize $icon_size icon-fig.png analysis.jpg
    fi
    


}
